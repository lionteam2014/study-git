<style>
/* 排序填位格*/
.sortable-placeholder {
	border: 2px dashed #CCC;
	background: none;
}
</style>

<script>
$(document).ready(function(){
	
	$('.sortable').sortable({
	    forcePlaceholderSize: true,
	    placeholder: $('<td colspan="3" class="sortable-placeholder">'),
	})
	.bind('sortupdate', function() {
	    // 排序後事件處理
	    var idSort = new Array();
		$('.sortable').find('tr').each(function(){
			idSort.push(this.id);
		});

		var data = {
			sort: JSON.stringify(idSort),
			type: 'sort',
		};

		$.blockUI({	 
	    	css:{ 
				border: 'none',
				padding: '15px', 
				backgroundColor: '#000',
				'border-radius': '10px',
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .5, 
				color: '#fff' 
		},message: '排序中...'});

		$.ajax({
			url: 'news/news/sort_action',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(response) {
				 $.unblockUI();
				 if (response.result) {
					    bootbox.alert('排序成功' ,function(){
					    	$("#page-wrapper").load("news/news");
						});
					}else{
						bootbox.alert('排序失敗!',function(){
					    	$("#page-wrapper").load("news/news");
						});
					}
			},
		});

	    
	});
	
});
</script>

<div class="row">
	<div class="col-sm-12">

		<button class="btn btn-primary js-load" id="add" data-toggle="tooltip" data-placement="bottom" title="新增" data-url="news/news/add"'><?=html_space(3)?><span class="glyphicon glyphicon-plus"></span><?=html_space(3)?></button>
		<?=html_space(4)?>
		
		<label>
			<input type="checkbox" id="checkbox-all" class="checkbox-css form-inline" name="checkbox-all"/><strong>&nbsp;&nbsp;全選</strong>
		</label>
		
		<?=html_space(4)?>
		
		<div class="btn-group">
			<button class="btn btn-danger btn-action checkbox-function hidden inline" id="delete" data-toggle="tooltip" data-placement="bottom" title="刪除"><?=html_space(4)?><span class="glyphicon glyphicon-trash"></span><?=html_space(4)?></button>
			<button class="btn btn-primary btn-action checkbox-function hidden inline" id="enable" data-toggle="tooltip" data-placement="bottom" title="顯示"><?=html_space(4)?><span class="glyphicon glyphicon-ok"></span><?=html_space(4)?></button>
			<button class="btn btn-primary btn-action checkbox-function hidden inline" id="disable" data-toggle="tooltip" data-placement="bottom" title="不顯示"><?=html_space(4)?><span class="glyphicon glyphicon-remove"></span><?=html_space(4)?></button>
		</div>
		
		<button class="btn btn-primary js-load pull-right" data-toggle="tooltip" data-placement="bottom" title="重新整理" data-url="news/news"><span class="glyphicon glyphicon-refresh"></span><?=html_space(1)?></button>

	</div>
</div>

<br />
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
		<form id='data' onsubmit="return false" enctype="multipart/form-data">
			<table class="table table-hover">
				<thead>
					<tr class='success'>
						<td width="60%" align="left"><input type="checkbox" id="checkbox-all" class="checkbox-css" name="checkbox-all"/><strong>&nbsp;&nbsp;主題</strong></td>
						<td width="20%" align="center"><strong>狀態</strong></td>
						<td width="20%" align="center"><strong>功能</strong></td>
					</tr>
				</thead>
				<tbody class="sortable">
<?php 
	if(count($data) == 0){
?>
		<td align="center" colspan="3"><strong class="text-danger">目前尚無項目 !</strong></td>
<?
	}else{

		foreach ($data as $obj) {
			$status_string = "顯示";
			$status_class = "text-primary";
			if(($obj->status == '0')){
				$status_string = "不顯示";
				$status_class = "text-danger";
			}
?>			
					<tr id="<?=$obj->sn?>" style="cursor:move;">
						<td align="left" >
							<input type="checkbox" id="<?=$obj->sn?>" name="checkbox-items[]" value="<?=$obj->sn?>" class='checkbox-item checkbox-css'/><label for="<?=$obj->sn?>">&nbsp;&nbsp;</label> 
							<span class="tooltip-item" data-toggle="tooltip" data-placement="right" title="檢視內容">
								<a href="#" class="text-info js-load" data-url="news/news/show?sn=<?=$obj->sn?>"><span class="glyphicon glyphicon-eye-open"></span> <?=$obj->subject?></a>
							</span>
						</td>
						<td align="center"><span class="<?=$status_class?>"> <?=$status_string?></span></td>
						<td align="center">
							<button class="btn btn-primary js-load" id="" data-toggle="tooltip" data-placement="bottom" title="修改" data-url="news/news/modify?sn=<?=$obj->sn?>" ><?=html_space(1)?><span class="glyphicon glyphicon-cog"></span><?=html_space(1)?></button>
						</td>
					
					</tr>
<?php 
		}
	}
?>			
				</tbody>
			</table>
		</form>
		</div>
	
	</div>
</div>


<script>

(function(){

	$('.tooltip-item').tooltip();
	
	$('.btn').tooltip();
	// hashAction.js 設定
	$(".js-load").loadPage('',$('#page-wrapper'));

})();

</script>

<script src="js/bootstrap.tablechbox.js"></script>
<script>
// 功能選單事件
$('.btn-action').on('click',function(event) {
	event.preventDefault();
	// 取得按鈕事件
	var url="news/news/selected_action",
	actionStr = '',
	action = this.id; 
	
	switch (action) {
	case 'delete':
		actionStr = '刪除';
		break;
	case 'enable':
		actionStr = '顯示';
		break;
	case 'disable':
		actionStr = '不顯示';
		break;
	default:
		break;
	}

	// 檢查有無被勾選的按鈕
	var checkbox_num = 0;
	$('input:checkbox.checkbox-item').each(function(){              
		if(this.checked == true){
			checkbox_num++;
		}
	});
	
	//如果有選取
	if(checkbox_num != 0){
		bootbox.confirm("你確定要 <span class='text-danger'>"+actionStr+"</span> 選取的 "+checkbox_num+ " 個項目嗎?", function(result) {
			if(result){
				// 在表單塞入動作
				$('<input>').attr({
				    type: 'hidden',
				    name: action,
				    value: true
				}).appendTo('#data');

				// 顯示阻擋UI
				blockUI(actionStr+'...');	 

				// 設定 jQuery.form 表單處理動作
				var options = {
						 url: url,
						 cache: false,
						 dataType: 'json',
						 type:'POST',
						 success: function(response) {
							 $.unblockUI();
							 if (response.result) {
								    bootbox.alert('成功 '+actionStr +" "+checkbox_num+" 個" ,function(){
								    	$("#page-wrapper").load("news/news");
									});
								}else{
									bootbox.alert(actionStr+'失敗，'+response.message,function(){
										$("#page-wrapper").load("news/news");
									});
								}
						}
				};
				// 送出表單動作
				$("#data").ajaxSubmit(options); 
				return ;
			}
	         return ;
			
		});

	}else{
		//顯示alert
		bootbox.alert('請勾選要操作項目');
		return false;
	}

});

</script>