<?php

/**
 * @author raybird
 *
 */
class About extends \Controller {
	
	/*
	 * (non-PHPdoc) @see Controller::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	/*
	 * (non-PHPdoc) @see Controller::after()
	 */
	public function after() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::destory()
	 */
	public function destory() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::before()
	 */
	public function before() {
		// TODO Auto-generated method stub
	}
	
	
	public function edit(){
		Loader::load("lionteam/php/about/AboutModel.php");
		$model = new AboutModel();
		$data = $model->getAbout();
		
		Loader::load("lionteam/php/about/AboutView.php");
		$view = new AboutView();
		$view->render("lionteam/php/about/views/edit.php", $data);
		
	}
}