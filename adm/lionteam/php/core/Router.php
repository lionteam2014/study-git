<?php
/**
 * 分析 URL 來決定該丟給哪個 package 操作
 * @author raybird
 *
 */
class Router {
	
	private function __construct(){}
	
	
	/**
	 * 需要初始的東西可以掛在這邊
	 */
	public static function init(){
		
		// 初始 session
		Session::init();
		
		// 字元過濾
		$_GET = Toolkit::handlerSlashes($_GET);
		$_POST = Toolkit::handlerSlashes($_POST);
		$_REQUEST = Toolkit::handlerSlashes($_REQUEST);
		
		// $_FILES 上傳目前無特別限制
	}
	
	/**
	 * 解析 url 來分發給 package 進行操作
	 */
	private static function _analyze_info($core_path, $path_info){
		
		// 參數處理
		$query_array = explode("/", $path_info);
		$package = $query_array[0];
		$control = isset($query_array[1])?ucfirst($query_array[1]):null;
		$method  = isset($query_array[2])?$query_array[2]:null;
		
		// 整理 control 路徑
		$path_control = $core_path.$package."/".$control.".php";

		if(file_exists($path_control)){
			// 載入 control
			require $path_control;
			
			// controller
			$controller = new $control();
			
			// 初始 model 與 view (此部分已取消轉由 Loader 處理 ) 因 IDE 無法參考到實際 class
			$controller->init($package, $control);
			
			// 執行 前置作業
			$controller->before();
			
			// 呼叫 control 方法
			if(isset($method)){
				
				// 檢查 controller 是否有定義此方法	
				if(method_exists($controller,$method)){
					$controller->{$method}();
				}else{
					BootstrapUIKit::alert($core_path, "control doesn't have this method.");
				}
				
			}else{
				$controller->index();
			}
			
			// 執行 後置作業
			$controller->after();
			
			// 結束 control 要關閉的資源
			$controller->destory();
		}else{	
			BootstrapUIKit::alert($core_path, "<h2>control is not exist.</h2>"."指定 control 不存在");	
		}
		
	}
	
	/**
	 * 執行 router 解析
	 */
	public static function exec($core_path=""){
		$path_info = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : (!empty($_SERVER['ORIG_PATH_INFO']) ? $_SERVER['ORIG_PATH_INFO'] : '');
		if(''!=$path_info){
			$path_info = substr($path_info, 1);
			self::_analyze_info($core_path, $path_info);
		}
	}
	
	/**
	 * 需要關閉可以在此關閉
	 */
	public static function destory(){
		
	}

}