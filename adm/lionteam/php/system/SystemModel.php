<?php

/**
 * @author raybird
 *
 */
class SystemModel extends \Model {
	
	/*
	 * (non-PHPdoc) @see Model::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	public function getSystem(){
		
		$pdo = DB::getInstance();
		
		$sql = "select * from _system limit 1";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $data[0];
	}
	
	public function updateDefault(){
		$pdo = DB::getInstance();
		
		$sql = "update _system set title=?, name_for_mail=?, email=?, meta_description=?, meta_keyword=? where sn=?";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute(array(
			$_POST["title"],
				$_POST["name_for_mail"],
				$_POST["email"],
				$_POST["meta_description"],
				$_POST["meta_keyword"],
				$_POST["sn"])
		);
		return $result;
	}
	
	public function updateEC(){
		$pdo = DB::getInstance();
		
		$sql = "update _system set allpay_client_id=?, allpay_hashkey=?, allpay_hashiv=?, atm_remark=? where sn=?";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute(array(
				$_POST["allpay_client_id"],
				$_POST["allpay_hashkey"],
				$_POST["allpay_hashiv"],
				$_POST["atm_remark"],
				$_POST["sn"])
		);
		return $result;
	}
	
}