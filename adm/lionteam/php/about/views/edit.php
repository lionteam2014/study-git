
<div class="row">
	<div class="col-sm-12  ">
		
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">關於我們</h3>
			</div>
			<div class="panel-body">

				<form class="form-horizontal" role="form" id="default_data" onsubmit="return false" enctype="multipart/form-data">
					<input type="hidden" name="sn" value="<?=$data->sn?>"/>
				
					<div class="form-group">
<!-- 						<label for="meta_description" class="col-sm-2 control-label">購物車下方編輯器</label> -->
						<div class="col-sm-12">
							<textarea class="form-control" rows="3" id="content" name="content"><?=$data->content?></textarea>
							<span class="help-block"></span>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="default_send" class="btn btn-success pull-right" data-toggle="tooltip" data-placement="bottom" title="送出"><?=html_space(3)?><span class="glyphicon glyphicon-save"><?=html_space(1)?></button>
						</div>
					</div>
				</form>

			</div>
		</div>

	</div>
	<script>
	(function(){

		var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
		editorHeight = h*0.6;
		
		var editor = CKEDITOR.replace( 'content',{
			height: editorHeight+'px',

		} );	

		CKFinder.setupCKEditor( editor, 'editor/ckfinder/' );

	})();
	</script>
	<script src="js/jquery.validate.min.js"></script>
	<script>
	
	$(function(){
		$("#default_send").on('click', function(e){
			//e.preventDefault();
			console.log('click');
			$('#default_data').validate({
			    rules: {
			    	title: {
			    		minlength: 1,
			            required: true,
			        },
			        name_for_mail: {
			    		minlength: 1,
			            required: true,
			        },
			        email: {
			    		minlength: 1,
			            required: true,
			        },
			    },
			    messages: {
					/*需驗證的欄位的錯誤訊息*/
					title: "請輸入 Title",
					name_for_mail: "請輸入名稱",
					email: "請輸入email"
				},
			    submitHandler: function (form){
			    	$.blockUI({	 
				    	css:{ 
							border: 'none',
							padding: '15px', 
							backgroundColor: '#000',
							'border-radius': '10px',
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
					},message: '修改中...'});
			    	
			    	var options = {
							 url: 'main.php?api=system/system/updateDefault',
							 cache: false,
							 dataType: 'json',
							 type:'POST',
							 success: function(response) {
								$.unblockUI();
								if(response.result) {
									bootbox.alert("修改成功", function(){
										//location.hash='main.php?api=system/system';
									});
								}else{
									bootbox.alert("修改成功!"+response.message, function(){
										location.hash='main.php?api=main.php?api=system/system';
									});
								}
							}
					}; 
					$("#default_data").ajaxSubmit(options); 
	                return false; 
			    }
			});
		});

	}());

	</script>
	
<script>
$(document).ready(function(){
	$('.btn').tooltip();
});
</script>