
<div class="row">
	<div class="col-sm-12  ">
		
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">基本設定</h3>
			</div>
			<div class="panel-body">

				<form class="form-horizontal" role="form" id="default_data" onsubmit="return false" enctype="multipart/form-data">
					<input type="hidden" name="sn" value="<?=$data->sn?>"/>
				
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label"><span class="glyphicon glyphicon-asterisk text-danger"></span>前台Title</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="title" value="<?=$data->title?>" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="name_for_mail" class="col-sm-2 control-label"><span class="glyphicon glyphicon-asterisk text-danger"></span>發信人名稱</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="name_for_mail" name="name_for_mail" value="<?=$data->name_for_mail?>" placeholder="管理員名稱" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label"><span class="glyphicon glyphicon-asterisk text-danger"></span>系統信箱</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="email" name="email" value="<?=$data->email?>" placeholder="email" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="meta_description" class="col-sm-2 control-label">網站描述</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="3" id="meta_description" name="meta_description"><?=$data->meta_description?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="meta_keyword" class="col-sm-2 control-label">網站關鍵字</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="3" id="meta_keyword" name="meta_keyword"><?=$data->meta_keyword?></textarea>
						</div>
					</div>
					<!-- 
					<div class="form-group">
						<label for="meta_description" class="col-sm-2 control-label">購物車感謝訊息</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="3" id="ec_remark" name="ec_remark"><?=$data->ec_remark?></textarea>
							<span class="help-block"></span>
						</div>
						
					</div>
					
					
					<div class="form-group">
						<label for="meta_description" class="col-sm-2 control-label">購物車下方編輯器</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="3" id="ec_editor" name="ec_editor"><?=$data->ec_remark?></textarea>
							<span class="help-block">顯示在購物車結束流程</span>
						</div>
					</div>
					 -->
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="default_send" class="btn btn-success pull-right" data-toggle="tooltip" data-placement="bottom" title="送出"><?=html_space(3)?><span class="glyphicon glyphicon-save"><?=html_space(1)?></button>
						</div>
					</div>
				</form>

			</div>
		</div>

	</div>
	<script>
	(function(){
		var editor = CKEDITOR.replace( 'ec_editor' );	
		CKFinder.setupCKEditor( editor, 'editor/ckfinder/' );

	})();
	</script>
	<script src="js/jquery.validate.min.js"></script>
	<script>
	
	$(function(){
		$("#default_send").on('click', function(e){
			//e.preventDefault();
			console.log('click');
			$('#default_data').validate({
			    rules: {
			    	title: {
			    		minlength: 1,
			            required: true,
			        },
			        name_for_mail: {
			    		minlength: 1,
			            required: true,
			        },
			        email: {
			    		minlength: 1,
			            required: true,
			        },
			    },
			    messages: {
					/*需驗證的欄位的錯誤訊息*/
					title: "請輸入 Title",
					name_for_mail: "請輸入名稱",
					email: "請輸入email"
				},
			    submitHandler: function (form){
			    	$.blockUI({	 
				    	css:{ 
							border: 'none',
							padding: '15px', 
							backgroundColor: '#000',
							'border-radius': '10px',
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
					},message: '修改中...'});
			    	
			    	var options = {
							 url: 'main.php?api=system/system/updateDefault',
							 cache: false,
							 dataType: 'json',
							 type:'POST',
							 success: function(response) {
								$.unblockUI();
								if(response.result) {
									bootbox.alert("修改成功", function(){
										//location.hash='main.php?api=system/system';
									});
								}else{
									bootbox.alert("修改成功!"+response.message, function(){
										location.hash='main.php?api=main.php?api=system/system';
									});
								}
							}
					}; 
					$("#default_data").ajaxSubmit(options); 
	                return false; 
			    }
			});
		});

	}());

	</script>
	
<!-- 
	<div class="col-sm-12  ">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">金流設定</h3>
			</div>
			<div class="panel-body">

				<form class="form-horizontal" role="form" id="ec_data" onsubmit="return false" enctype="multipart/form-data">
					<input type="hidden" name="sn" value="<?=$data->sn?>"/>
					<div class="form-group">
						<label for="allpay_client_id" class="col-sm-2 control-label"><span class="glyphicon glyphicon-asterisk text-danger"></span>歐付寶 client id</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="allpay_client_id" name="allpay_client_id" value="<?=$data->allpay_client_id?>" placeholder="client id" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="allpay_hashkey" class="col-sm-2 control-label"><span class="glyphicon glyphicon-asterisk text-danger"></span>hashkey</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="allpay_hashkey" name="allpay_hashkey" value="<?=$data->allpay_hashkey?>" placeholder="hashkey" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="allpay_hashiv" class="col-sm-2 control-label"><span class="glyphicon glyphicon-asterisk text-danger"></span> hashiv</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="allpay_hashiv" name="allpay_hashiv" value="<?=$data->allpay_hashiv?>" placeholder="hashiv" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="meta_description" class="col-sm-2 control-label">購物車結束訊息</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="3" id="atm_remark" name="atm_remark"><?=$data->ec_remark?></textarea>
							<span class="help-block">顯示在購物車使用ATM付款的第三步與訂單查詢</span>
						</div>
						
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="ec_send" class="btn btn-success pull-right" data-toggle="tooltip" data-placement="bottom" title="送出"><?=html_space(3)?><span class="glyphicon glyphicon-save"><?=html_space(1)?></button>
						</div>
					</div>
				</form>

			</div>
		</div>

	</div>
	
</div>
 -->	


<script>
$(function(){
	$("#ec_send").on('click', function(e){
		//e.preventDefault();
		console.log('click');
		$('#ec_data').validate({
		    rules: {
		    	allpay_client_id: {
		    		minlength: 1,
		            required: true,
		        },
		        allpay_hashkey: {
		    		minlength: 1,
		            required: true,
		        },
		        allpay_hashiv: {
		    		minlength: 1,
		            required: true,
		        },
		        atm_remark: {
		    		minlength: 1,
		            required: true,
		        },
		    },
		    messages: {
				/*需驗證的欄位的錯誤訊息*/
				allpay_client_id: "請輸入歐付寶 client id",
				allpay_hashkey: "請輸入歐付寶 hashiv",
				allpay_hashiv: "請輸入歐付寶 hashiv",
				atm_remark: "請顯示在購物車使用ATM付款的第三步與訂單查詢"
			},
		    submitHandler: function (form){
		    	$.blockUI({	 
			    	css:{ 
						border: 'none',
						padding: '15px', 
						backgroundColor: '#000',
						'border-radius': '10px',
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
				},message: '修改中...'});
		    	
		    	var options = {
						 url: 'main.php?api=system/system/updateEC',
						 cache: false,
						 dataType: 'json',
						 type:'POST',
						 success: function(response) {
							$.unblockUI();
							if(response.result) {
								bootbox.alert("修改成功", function(){
									//location.hash='main.php?api=system/system';
								});
							}else{
								bootbox.alert("修改成功!"+response.message, function(){
									location.hash='main.php?api=main.php?api=system/system';
								});
							}
						}
				}; 
				$("#ec_data").ajaxSubmit(options); 
                return false; 
		    }
		});
	});

}());


</script>


<script>
$(document).ready(function(){
	$('.btn').tooltip();
});
</script>