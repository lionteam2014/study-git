<?
/**
* Light MVC Base on Apache rewrite 
* @author raybird
*/
// if(array_key_exists("PATH_INFO", $_SERVER)){
$path_info = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : (!empty($_SERVER['ORIG_PATH_INFO']) ? $_SERVER['ORIG_PATH_INFO'] : '');
if(''!=$path_info){
	include 'app.php';
}else{
	include 'app.php';
	// 有登入就去 main.php，否去登入頁面
	if( Session::get("isLogin") ){
		header("Location: main.php");
		
	}else{
		include 'login.html';
	}
}
?>