<?php
/**
 * 預設登入 base 頁面。
 * 這個頁面是由 page script 的方式撰寫
 * */ 
include_once 'app.php';

if( !Session::get("isLogin") ){
	Session::destroy();
	header('Location: logout.php');
}

require 'lionteam/php/system/System.php';
$system = new System();
$systemData = $system->getSystem();




?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - SB Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="main.php">管理後臺</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
         
          <ul class="nav navbar-nav side-nav">
          	
            <li class="active js-load menu-list" data-url="system/system"><a href="#"><i class="icon-dashboard"></i> 系統設定</a></li>
            <li class="js-load menu-list" data-url="manager/manager"><a href="#"><i class="icon-edit"></i> 帳號管理</a></li>
            <li class="js-load menu-list" data-url="news/news"><a href="#"><i class="icon-edit"></i> 最新消息</a></li>
            <li class="js-load menu-list" data-url="about/about/edit"><a href="#"><i class="icon-bar-chart"></i> 關於我們</a></li>
            <!-- 
            <li><a href="typography.html"><i class="icon-font"></i> Typography</a></li>
            <li><a href="bootstrap-elements.html"><i class="icon-desktop"></i> Bootstrap Elements</a></li>
            <li><a href="bootstrap-grid.html"><i class="icon-wrench"></i> Bootstrap Grid</a></li>
            <li><a href="blank-page.html"><i class="icon-file-alt"></i> Blank Page</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-collapse"></i> Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Dropdown Item</a></li>
                <li><a href="#">Another Item</a></li>
                <li><a href="#">Third Item</a></li>
                <li><a href="#">Last Item</a></li>
              </ul>
            </li>
             -->
          </ul>


          <ul class="nav navbar-nav navbar-right navbar-user">
          <!-- 
            <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope"></i> Messages <span class="badge">7</span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">7 New Messages</li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="icon-time"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="icon-time"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="icon-time"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li><a href="#">View Inbox <span class="badge">7</span></a></li>
              </ul>
            </li>
            <li class="dropdown alerts-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-bell-alt"></i> Alerts <span class="badge">3</span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Default <span class="label label-default">Default</span></a></li>
                <li><a href="#">Primary <span class="label label-primary">Primary</span></a></li>
                <li><a href="#">Success <span class="label label-success">Success</span></a></li>
                <li><a href="#">Info <span class="label label-info">Info</span></a></li>
                <li><a href="#">Warning <span class="label label-warning">Warning</span></a></li>
                <li><a href="#">Danger <span class="label label-danger">Danger</span></a></li>
                <li class="divider"></li>
                <li><a href="#">View All</a></li>
              </ul>
            </li>
            -->
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> 管理員 <b class="caret"></b></a>
              <ul class="dropdown-menu">
<!--                 <li><a href="#"><i class="icon-user"></i> Profile</a></li> -->
<!--                 <li><a href="#"><i class="icon-envelope"></i> Inbox <span class="badge">7</span></a></li> -->
<!--                 <li><a href="#"><i class="icon-gear"></i> Settings</a></li> -->
<!--                 <li class="divider"></li> -->
                <li><a href="logout.php"><i class="icon-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
          
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper" class="well">

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.form.js"></script>
	<script src="js/jquery.validate.min.js"></script>
    <script src="js/jquery.blockUI.js"></script>
    <script src="js/jquery.sortable.update.min.js"></script>
    <script src="js/jquery.hashAction.js?date=<?=date("mdHis")?>"></script>
    
    <script src="editor/ckeditor/ckeditor.js"></script>
	<script src="editor/ckfinder/ckfinder.js"></script>
    <!-- Page Specific Plugins -->
    <!-- 
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <script src="js/morris/chart-data-morris.js"></script>
    <script src="js/tablesorter/jquery.tablesorter.js"></script>
    <script src="js/tablesorter/tables.js"></script>
     -->
    <script>
	$(document).ready(function(){
		// 頁面重新整理時，主畫面要顯示的view
		var $main = $('#page-wrapper'),
		current_hash = window.location.hash.slice(1); // 取得 url 上的 hash 直用來當使用者重新整理頁面

		if(current_hash == ""){
			// 載入 index 頁面
			$main.load('system/system');
		}else{
			// 載入目標 頁面
			$main.load(current_hash);
		}

	});
    </script>
    
    <script>
	(function(){
		// 當顯示手機模式時 功能選單點擊後沒有收回 nabar-collapse 事件
		$('.menu-list')
		.on('click',function(){
			var $navbar_collapse = $('.navbar-collapse'),
			viewWidth = $(window).width();
			
			if(viewWidth < 768){
				$navbar_collapse.collapse('toggle');
			}
		});

	})();
    </script>
    
    <script>
	$(function($){
		// hashAction.js 設定
		$(".js-load").loadPage('',$('#page-wrapper'));

	}($));

    </script>
    
    <script>
	$(function(){
		// 功能選單按下後設定為 active
		$(".menu-list").on("click",function(){
			var $that = $(this);

			$(".menu-list").each(function(){
				var $this = $(this);
				$this.removeClass("active");
			});
			$that.addClass("active");

		});
	}());
    </script>
    <script>
    /** 統一 blockUI 效果*/
	function blockUI(message){
    	$.blockUI({	 
	    	css:{ 
				border: 'none',
				padding: '15px', 
				backgroundColor: '#000',
				'border-radius': '10px',
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .5, 
				color: '#fff' 
				},
			message: message
		});
	}

    </script>
  </body>
</html>
