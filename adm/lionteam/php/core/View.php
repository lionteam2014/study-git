<?php
/**
 * 
 * @author raybird
 *
 */
class View {
	
	public function __construct(){}
	
	/**
	 * 秀出 view 畫面
	 * @param unknown $name
	 */
	public function render($path, $data=null){
		$filename = $path;
		if(file_exists($filename)){
			include $filename;
// 			echo FishUI::mainFooterTime();
		}else{
			echo $filename ." 不存在";
		}
	}
	
}