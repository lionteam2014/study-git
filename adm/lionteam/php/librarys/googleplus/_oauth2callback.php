<?php 
/**
 * 驗證 google 帳號
 * 參考:
 * https://developers.google.com/+/web/signin/?hl=zh-tw
 * 2014/02/13
 * @author kevin
 */
session_start ();

include ("core/set_config.php");

header("Content-Type:text/html; charset=utf-8");


if($_GET["action"]=="login_success"){
	
	$gmail = $_GET["gmail"];
	$email = $_GET["email"];
	
	if($email == $_SESSION["email"]){
		$sql = "UPDATE `sell_seller` SET  `gmail` = '$gmail', `gmail_auth`='1'  WHERE  `email` ='$email';";
		$DB_class->DB_query($sql);
		$respones = array("result"=>true);
		
	}else{
		$respones = array("result"=>false);
	}
	echo json_encode($respones);
	exit();
}



?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>google oauth</title>
</head>
<body>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
		function getParams(){
			var params = {}, queryString = location.hash.substring(1),
		    regex = /([^&=]+)=([^&]*)/g, m;
			while (m = regex.exec(queryString)) {
			  params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
			}
			return params;
		}

		function authGmail(gmail, email){
			var url = "oauth2callback.php?action=login_success&gmail="+gmail+"&email="+email;
			
			$.ajax({
			    type: 'GET',
			    url: url,
			    contentType: "application/json",
			    dataType: 'json',
			    success: function(response) {
				    if(response.result){
				    	alert("已完成 Google 服務驗證，請重新登入!");
					    $('<a>',{
					        text: '已完成 Google 服務驗證，請重新登入!',
					        title: '重新登入',
					        href: 'index.php'
					    }).appendTo('body');
				    	//location.href="index.php";
					}
				    
			    },
			    error: function(e) {
			      // Handle the error
// 			      console.log("error:"+e);
			      // You could point users to manually disconnect if unsuccessful
			      // https://plus.google.com/apps
			      location.href="index.php";
			    }
			  });
		}

		$(window).load(function(){
			var params= getParams(),
			url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token="+params['access_token'];
			$.ajax({
			    type: 'GET',
			    url: url,
			    async: false,
			    contentType: "application/json",
			    dataType: 'json',
			    success: function(response) {
// 			    	console.log(response);
			    	authGmail(response.email, '<?=$_SESSION["email"]?>');			    	
			    },
			    error: function(e) {
			      // Handle the error
			      console.log(e);
			      // You could point users to manually disconnect if unsuccessful
			      // https://plus.google.com/apps
			      //location.href="index.php";
			    }
			  });
		});
 		
		
		
		
	});
	</script>
	
</body>
</html>