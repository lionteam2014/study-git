/**
 * 列表頁勾選效果
 */
$(document).ready(function(){
	/*全選*/
	$("#checkbox-all").click(function() {
	   if($("#checkbox-all").prop("checked")) {
	     $("input:checkbox.checkbox-css").each(function() {
	         $(this).prop("checked", true);
	         if(this.id !='checkbox-all'){
	        	 $(this).closest('tr').addClass('warning');
	        	 $(this).closest('.panel').removeClass('panel-default').addClass('panel-info');
	         }	         
	     });
	   } else {
	     $("input:checkbox.checkbox-css").each(function() {
	         $(this).prop("checked", false);
	         $(this).closest('tr').removeClass('warning');
	         $(this).closest('.panel').removeClass('panel-info').addClass('panel-default');
	     });           
	   }
	   checkFunctionHandler();
	});

	$("input:checkbox.checkbox-item").on('click',function() {
		var that = $(this);
		if(that.prop("checked")){
			$(this).closest('tr').addClass('warning');
			$(this).closest('.panel').removeClass('panel-default').addClass('panel-info');
			var all = $("input:checkbox.checkbox-item").length;
			
			// 檢查有無被勾選的按鈕
			var checkbox_num = 0;
			$('input:checkbox.checkbox-item').each(function(){              
				if(this.checked == true){
					checkbox_num++;
				}
			});
			
			
			if(all == checkbox_num){
				$("#checkbox-all").prop("checked", true);
				
			}else{
				$("#checkbox-all").prop("checked", false);
				
			}
		}else{
			$("#checkbox-all").prop("checked", false);
			$(this).closest('tr').removeClass('warning');
			$(this).closest('.panel').removeClass('panel-info').addClass('panel-default');
		}
		checkFunctionHandler();
	});
	
	function checkFunctionHandler(){
		// 檢查有無被勾選的按鈕
		var checkbox_num = 0;
		$('input:checkbox.checkbox-item').each(function(){              
			if(this.checked == true){
				checkbox_num++;
			}
		});
//		console.log('檢查有無被勾選的按鈕:'+checkbox_num);
		if(checkbox_num>0){
//			console.log(checkbox_num);
			$('.checkbox-function').removeClass('hidden').addClass('show');
		}else{
			$('.checkbox-function').removeClass('show').addClass('hidden');
		}
	}
	
});