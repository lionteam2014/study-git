<?php
/**
 * 定義 Controller 類別
 * @author raybird
 *
 */
abstract class Controller{
	
	public function __construct(){}
		
	/**
	 * 初始作業
	 * @param unknown $package router 分發 package
	 * @param unknown $control package 要使用的 control
	 */
	public function init($package, $control){
		$this->package = $package; 
		$this->control = ucfirst($control); // 預設  Controller name
		$this->model = ucfirst($control)."Model"; // 預設 Model name
		$this->view = ucfirst($control)."View"; // 預設 View name
	}
	
	/**
	 * 呼叫方法前置作業，可再此處擴充 session 機制
	 */
	abstract public function before();
	
	/**
	 * 呼叫方法的後置作業
	 */
	abstract public function after();
	
	/**
	 * 結束作業
	 */
	abstract public function destory();
	
	/**
	 * 無給定 control 動作，就呼叫 index
	 */
	public function index(){
		$_view = Loader::loadClass("$this->package/$this->view.php", $this->view);
		$_view->render($this->package."/views/index.php");
	}
	
	/**
	 * 簡單回應操作結果
	 * @param unknown $result
	 * @param string $message
	 */
	protected function responseSimpleJson($result, $message=""){
		if($result){
			$response = array("result"=>true, "message" => $message);
		}else{
			$response = array("result"=>false, "message" => $message);
		}
		echo json_encode($response);
	}
	
}