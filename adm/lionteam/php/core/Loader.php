<?php

/**
 * 物件載入器，用來載入 Class 用
 * 要用到的非核心 class 在由此動態掛載，節省不需要掛載的 class 
 * @author raybird
 *
 */
class Loader {
	
	private function __construct(){}
	
	private static $_library_array = array();
	private static $_class_array = array();
	private static $_path_array = array();
	
	/**
	 * 載入 library 物件
	 * @param unknown $className librarys class name.
	 * @return Ambigous <multitype:, string>
	 */
	public static function loadLibrary($className){
		$className = ucfirst($className);
		if(in_array($className, self::$_library_array)){
			return self::$_library_array[$className];
		}
		$filename = 'librarys/'.$className.".php";
		if(file_exists($filename)){
			require $filename;
			$classObj = new $className();
			self::$_library_array[$className]=$classObj;
			return $classObj;
		}else{
			echo "$filename 沒有此library ";
		}
		
	}
	
	/**
	 * 載入class，並回傳已實例之物件。													<br />
	 * 使用說明:															<br />
	 * $obj = Loader::loadClass("demo/DemoModel.php", "DemoModel"); <br />
	 * 即可透過 $obj(名稱可自取) 操作使用 DemoModel 的物件。						<br />
	 * 缺點因為實例 class 是動態參數，造成 IDE 無法找到 reference class, so can't intelligent autoComplete 
	 * @param String $path 檔案路徑
	 * @param String $className class 名稱
	 * @return multitype:
	 */
	public static function loadClass($path, $className){
		$className = ucfirst($className);
		if(in_array($className, self::$_class_array)){
			return self::$_class_array[$className];
		}
		$filename = $path;
		if(file_exists($filename)){
			require $filename;
			$classObj = new $className();
			self::$_class_array[$className]=$classObj;
			return $classObj;
		}else{
			echo "$filename 沒有此 Class ";
		}
	}
	
	/**
	 * 載入此 class 路徑。
	 * 使用說明:
	 * Loader::loadClass("demo/DemoModel.php");
	 * $obj = new DemoModel();
	 * 因為 class 名稱非動態，IDE 開發時可以使用到 autoComplete
	 * @param unknown $path 要載入 class 檔案。
	 * @return boolean
	 */
	public static function load($path){
		
		if(in_array($path, self::$_path_array)){
			return ;
		}
		
		if(file_exists($path)){
			require $path;
			array_push(self::$_path_array, $path);
			return ;
		}else{
			echo "$path 沒有此 Class ";
			throw new Exception("$path 沒有此 Class ");
			return ;
		}
		
	}
	
}