<?php
/**
 * 2. 銀行確認頁面。
 * 只需要給銀行設定此頁面。確認此刷卡訂單是否存在並將此訂單改為已付款或付款失敗。
 */
include 'Creditcard.php';

$xml = $_POST["strRsXML"]; // 銀行會丟刷卡結果的 xml 過來

// 確認此訂單狀況
$xmlObj = Creditcard::xmltoobj($xml);

$STOREID = (string)$xmlObj->ORDERINFO->STOREID;
$ORDERNUMBER = (string)$xmlObj->ORDERINFO->ORDERNUMBER;
$AMOUNT = (string)$xmlObj->ORDERINFO->AMOUNT;
$AUTHSTATUS = (string)$xmlObj->AUTHINFO->AUTHSTATUS;
$CAVALUE = (string)$xmlObj->ORDERINFO->CAVALUE;

$option = array();

if($STOREID == '210050011'){ // 未分期
	$option = array(
			'store_id' => '210050011',
			'cubkey' => 'adbc35a8be097f28f0c89f03733e5f06'
	);
}else if($STOREID == '210050013'){
	$option = array(
			'store_id' => '210050013',
			'cubkey' => 'a880e91a5e11c0ead2d70738217fe2b1'
	);
}


$creditor = new Creditcard($option);

$result = false;
// 商店正確且刷卡成功，則更新資料庫
if($AUTHSTATUS=="0000"){
	// 請確認資料庫確定有此筆資料，將此比交易設定為成功
	/*
	 ....資料庫操作
	 */
	$result = true;
}else{
	// 請將此比對資料庫若有此筆訂單則把該筆訂單改成刷卡失敗
	/*
	 ....資料庫操作
	*/
}


// 回應給銀行轉導結果頁面。
$domain = 'mashup.com.tw';
if($result){
	$page = 'creditcard/result.php?result=ok';
	$end_page = $creditor->go_result($domain, $page);
}else{
	$page = 'creditcard/result.php?result=error';
	$end_page = $creditor->go_result($domain, $page);
}

echo $end_page;