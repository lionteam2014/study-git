<?
session_start();
/**
 * 獨立出處理 facebook 登入機制
 * 正在整理中未調整完畢
 * */
class FacebookService {
	/** 主要負責的頁面導向 javascript ，處理 json 物件 */
	var $jsonHandler = "js_facebook_handler.php";
	
	/** 應用程式 ID / API 鑰匙 */
	var $app_id = "134951176683872";
	 
	/** 應用程式密鑰 */
	var $app_secret = "8ec5a9f44e224b4e152e92c84e39355c";
	
	/**
	 * 可以要求使用者未公開資訊，會讓使用者自行確認同意後才可取得。  </br>
	 * 例如: scope=user_birthday,read_stream   </br>
	 * 官方說明頁:https://developers.facebook.com/docs/concepts/login/permissions-login-dialog/
	 */
	var $scope = "email"; // 要取得使用者email
	
	var $json;
	

	/**
	 * facebook 登入的URL
	 * @param unknown $code
	 * @return string
	 */
	public function getFacebookLoginRedectorURL($code){
		if(empty($code)) {
			$lastndexOf = strrpos($_SERVER['PHP_SELF'],"/",0);
			$dir = substr($_SERVER['PHP_SELF'],0,$lastndexOf);
			
			// facebook 登入後要轉導頁面
			$redirect= "http://".$_SERVER['HTTP_HOST'].$dir."/".$this->jsonHandler;
	
			// Redirect to Login Dialog
			$redector = "https://www.facebook.com/dialog/oauth?client_id=".$this->app_id."&redirect_uri=$redirect&state=".$_SERVER['PHP_SELF']."&scope=".$this->scope;
		}
		return $redector;
	}
	
	/**
	 *  取得 登入後使用者 json 字串
	 * @param unknown $code Facebook 登入成功後轉回頁面會帶的 $_GET['code'] 參數
	 * @return string Facebook response json 
	 */
	public function getFacebookUserJson($code){
		if($code)
		{
			$lastndexOf = strrpos($_SERVER['PHP_SELF'],"/",0);
			$dir = substr($_SERVER['PHP_SELF'],0,$lastndexOf);
			$parentURL= "http://".$_SERVER['HTTP_HOST'].$dir; // 路徑紀錄
			$redirect= $parentURL."/".$this->jsonHandler;
			// 取得fb access_token
			$token_url="https://graph.facebook.com/oauth/access_token?client_id=".$this->app_id."&redirect_uri=".$redirect."&client_secret=".$this->app_secret."&code=".$code;
			$response = file_get_contents($token_url);
			$params = NULL;
			parse_str($response, $params);
	
			$_SESSION['access_token'] = $params['access_token'];
	
			$graph_url = "https://graph.facebook.com/me?access_token=".$params['access_token'];
			// 取得使用者資訊
			$this->json = file_get_contents($graph_url);
			
			return $this->json;
		}
	}
	
	/**
	 * 可以針對 json 做處裡，寫入到資料庫後轉導頁面等等的，自行擴充內容
	 */
	 public function facebookJsonHandler(){
	 	$obj = json_decode($this->json);
	 	/** 可以在此寫入資料庫等等
	 	 * ....
	 	 * ....
	 	 */
	 	return $obj;
	 }	 
}

// 使用js處理(原本)
$jsFBService = new FacebookService();

// 指定處理頁面
$customFBService = new FacebookService();
$customFBService->jsonHandler="php_facebook_handler.php";
