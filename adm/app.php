<?php 
/**
 * 利用 Apache rewrite 做簡易的 MVC 架構。
 * 
 * index.php/package/controller/method
 * 
 * path_info = /package/controller/method
 * 
 * package 說明:
 * 
 * 	-lionteam/php/core
 * 	核心類別
 * 
 * 	-lionteam/php/librarys
 * 	外掛工具
 * 
 *  -lionteam/php/*
 *  各個獨立的 package 幾乎對應一個功能項目
 *  
 *  
 *  由 Router 此類別依據 path_info 進行分配 Package/controller/method 的載入與呼叫
 *  controller 使用 Loader 進行最小載入(require)需要的 php 檔案。
 *  
 *  
 * 
 * @author raybird
 */
// 各PHP版本顯示所有錯誤的配置
// < 5.3 -1 or E_ALL
// 5.3 -1 or E_ALL | E_STRICT
// > 5.3 -1 or E_ALL
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('date.timezone','Asia/Taipei');
header("Content-Type:text/html; charset=utf-8");
// echo $time = date("Y-m-d H:i:s");

ob_start ();

// 定義 Router 解析的 get params
define('PARAMS','api');

// 定義資料庫
if( strpos($_SERVER['HTTP_HOST'],'localhost') !== false ){
	define('DB_DSN', 'mysql:dbname=youlong;host=localhost');
	define('DB_USER','raybird');
	define('DB_PASS', 'mysql783688');
}

// 定義資料庫
if( strpos($_SERVER['HTTP_HOST'],'no-ip') !== false ){
	define('DB_DSN', 'mysql:dbname=youlong;host=localhost');
	define('DB_USER','raybird');
	define('DB_PASS', 'mysql783688');
}


// 後臺資料夾 lionteam/php，不使用變數讓 Eclipse 能參考到有利開發速度

// core
require 'lionteam/php/core/DB.php';
require 'lionteam/php/core/Controller.php';
require 'lionteam/php/core/Model.php';
require 'lionteam/php/core/View.php';
require 'lionteam/php/core/Router.php';
require 'lionteam/php/core/Loader.php';
require 'lionteam/php/core/Session.php';

// library
include 'lionteam/php/librarys/Functions.php';
require 'lionteam/php/librarys/Toolkit.php';
require 'lionteam/php/librarys/bootstrap3ui/BootstrapUIKit.php';


// 初始機制，可在這邊掛載安全機制例如字元過濾或者 logger 機制
Router::init();

// 執行 request，並送出 response
Router::exec("lionteam/php/");

// 關閉需要關閉的東西
Router::destory();
