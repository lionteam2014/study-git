<?php

/**
 * @author raybird
 *
 */
class NewsModel extends \Model {
	
	/*
	 * (non-PHPdoc) @see Model::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	/**
	 * 取得所有資料
	 * @return multitype:
	 */
	public function getAllNews(){
		
		$sql = "select * from news order by sort, sn desc";
		
		$pdo = DB::getInstance();
		
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		return $data;
	}
	
	
	public function addNews(){
		
		$sql = "insert into news (subject, content, status, post_time, sort) 
				values (?, ?, ?, ?, ?)";
		
		$pdo = DB::getInstance();
		
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute(array(
				$_POST["subject"],
				$_POST["content"],
				$_POST["status"],
				date(DateTime::W3C),
				0
		));
		
		return $result;
	}
	
	/**
	 * 新增或修改資料
	 * @return unknown|boolean
	 */
	public function saveNews(){
	
		if(array_key_exists("sn", $_POST) && intval($_POST["sn"]) > 0 ){
			// 修改				
			$sql = "update news set subject=?, content=? ,status=? ,post_time=? where sn=? " ;
				
			$stmt = DB::prepare($sql);
			$result = $stmt->execute(array(
					$_POST["subject"], 
					$_POST["content"], 
					$_POST["status"], 
					date(DateTime::W3C), 
					$_POST["sn"]));
			return $result;
		}else{
			// 新增
			$sql = "insert into news (subject, content, status, post_time, sort) 
				values (?, ?, ?, ?, ?)";
		
			$pdo = DB::getInstance();
			
			$stmt = $pdo->prepare($sql);
			$result = $stmt->execute(array(
					$_POST["subject"],
					$_POST["content"],
					$_POST["status"],
					date(DateTime::W3C),
					0
			));
			
			return $result;
				
		}
	}
	
	
	/**
	 * 取得一筆資料
	 * @return Ambigous <>
	 */
	public function getNews(){
				
		$sql = "select * from news where sn=?";
		
		$pdo = DB::getInstance();
		
		$stmt = $pdo->prepare($sql);
		$stmt->execute(array($_GET["sn"]));
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		return $data[0];
		
	}
	
	/**
	 * 進行多選的操作功能
	 * @return boolean
	 */
	public function selected_action(){
	
		$itmes = $_POST["checkbox-items"];
	
		if(array_key_exists("enable", $_POST) &&  $_POST["enable"] == true){
	
			foreach ($itmes as $sn) {
				$sql = "update news set status=?  where sn=?";
				$stmt = DB::prepare($sql);
				$result = $stmt->execute(array(1, $sn));
			}
	
			return true;
		}else if(array_key_exists("disable", $_POST) && $_POST["disable"] == true){
	
			foreach ($itmes as $sn) {
				$sql = "update news set status=?  where sn=?";
				$stmt = DB::prepare($sql);
				$result = $stmt->execute(array(0, $sn));
			}
	
			return true;
		}else if(array_key_exists("delete", $_POST) && $_POST["delete"] == true){
	
			foreach ($itmes as $sn) {
	
				$sql = "delete from news where sn=?";
				$stmt = DB::prepare($sql);
				$result = $stmt->execute(array($sn));
			}
	
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 進行排序
	 * @return boolean
	 */
	public function sort(){
		$sort = json_decode(stripslashes($_POST["sort"]));
		if(array_key_exists("type", $_POST) &&  $_POST["type"] == 'sort'){
			$num = 1;
				
			foreach ($sort as $sn) {
				$sql = "update news set sort=?  where sn=?";
				$stmt = DB::prepare($sql);
				$result = $stmt->execute(array($num, $sn));
	
				$num++;
			}
	
			return true;
		}else{
			return false;
		}
	
	}
}