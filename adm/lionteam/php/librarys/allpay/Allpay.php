<?php

/**
 * 歐付寶 全功能金流整合 v1.1.2
 * 
 * 參數與方法都 public static 可直接修改呼叫。
 * 建議透過 Allpay::init($options) 做初始動作，再進行呼叫。
 * 
 * 參考文件:全功能金流整合介接規格說明_20131025230906.pdf
 * @link 文件來源: https://member.allpay.com.tw/PaymentCenter/ListAPIDoc
 * 
 * @author raybird
 *
 */
class Allpay {
	
	/** 正式環境(請注意:正式機為https，測試機則為http) */
	const URL = "https://payment.allpay.com.tw/Cashier/AioCheckOut";
	/** 測試環境(測試環境需跟歐付寶提出申請，方可使用) */
	const TESTURL = "http://payment-stage.allpay.com.tw/Cashier/AioCheckOut";
	
	public static $HashKey = "5294y06JbISpM5x9";
	
	public static $HashIV = "v77hoKGq4kWxNNIS";
	
	
	/**
	 * 廠商編號(由AllPay提供)	
	 * Varchar(10) 							
	 * 不可為空。	
	 * 123456789
	 * @var unknown
	 */
	public static $MerchantID = "2000132"; // test
	/**
	 * 廠商交易編號  			
	 * Varchar(20) 
	 * 廠商交易編號不可重覆。				
	 * 不可為空。	
	 * Allpay_1234
	 * @var unknown
	 */
	public static $MerchantTradeNo;
	/**
	 * 廠商交易時間 			
	 * Varchar(20) 
	 * 格式為: yyyy/MM/dd HH:mm:ss 	
	 * 不可為空。	
	 * 2012/03/21 15:40:18 
	 * @var unknown
	 */
	public static $MerchantTradeDate;
	/**
	 * 交易類型 				
	 * Varchar(20)
	 * 請帶 aio						
	 * 不可為空。	
	 * aio
	 * @var unknown
	 */
	public static $PaymentType = "aio";
	/**
	 * 交易金額 				
	 * Money 									
	 * 不可為空。	
	 * 5000
	 * @var unknown
	 */
	public static $TotalAmount;
	/**
	 * 交易描述 				
	 * Varchar(200)							
	 * 不可為空。	
	 * Allpay商城購物-
	 * @var unknown
	 */
	public static $TradeDesc = "Mashup 歐付寶付費";
	/**
	 * 商品名稱	 			
	 * Varchar(200)	
	 * 如果商品名稱有多筆，需在金流選擇頁一行一行顯示商品名稱的話，商品名稱請以井號分隔(#) 
	 * 不可為空。	
	 * 手機20元X2#隨身碟60元X1 
	 * @var unknown
	 */
	public static $ItemName;
	/**
	 * 回傳網址				
	 * Varchar(200)	
	 * 當消費者付款完成後，會將付款結果回傳到該網址。
	 * 不可為空。	
	 * http://www.allpay.com.tw/receive.php
	 * @var unknown
	 */
	public static $ReturnURL = "url/ReturnURL.php";
	/**
	 * 選擇預設付款方式
	 * Varchar(20) 	
	 * 歐付寶提供下列付款方式，請於建立訂單時傳送過來: 
	 * Credit:信用卡 
	 * WebATM:網路ATM 
	 * ATM:自動櫃員機 
	 * CVS:超商代碼 
	 * BARCODE:超商條碼 
	 * Alipay:支付寶 
	 * Tenpay:財付通 
	 * TopUpUsed:儲值消費 
	 * ALL:不指定付款方式，由歐付寶顯示付款方式選擇頁面。
	 * 當DeviceSource為M時，ChoosePayment請帶 ALL給歐付寶。
	 * 不可為空 
	 * WebATM 
	 * @var unknown
	 */
	public static $ChoosePayment = "ALL";
	/**
	 * 檢查碼				 
	 * Varchar 		
	 * 請參考附錄檢查碼機制			
	 * 不可為空 	
	 * @var unknown
	 */
	public static $CheckMacValue;
	/**
	 * Client端回傳網址 		
	 * Varchar(200) 	
	 * 此網址為付款完成後，銀行將頁面導回到歐付寶時，歐付寶會顯示付款完成頁，該頁面上會有[回到廠商]的按鈕，會員點選按鈕後，會將頁面導回到此設定的網址。	
	 * 可為空 
	 * http://www.allpay.com.tw/Shopping/Detail 
	 * @var unknown
	 */
	public static $ClientBackURL;
	/**
	 * 商品銷售網址
	 * Varchar(200)
	 * 可為空
	 * @var unknown
	 */
	public static $ItemURL;
	/**
	 * 備註欄位。 
	 * Varchar(100) 
	 * 目前都請放空白。 
	 * 可為空
	 * @var unknown
	 */
	public static $Remark ;
	/**
	 * 選擇預設付款子項目
	 * Varchar(20)
	 * 若正確設定此欄位，使用者則無法看見金流選擇頁，直接使用設定的付款方式，但信用卡(Credit)與儲值消費(TopUpUsed)無此功能。
	 * 例如：ChoosePayment設定WebATM，ChooseSubPayment設定TAISHIN，此次交易就會以台新銀行的網路ATM付款。請參考付款方式一覽表
	 * 可為空
	 * TAISHIN
	 * @var unknown
	 */
	public static $ChooseSubPayment;
	
	/**
	 * Client端回傳付款結果網址 
	 * Varchar(200) 
	 * 此網址為付款完成後，銀行將頁面導回到歐付寶時，會將頁面導回到此設定的網址，並帶回付款結果的參數，沒帶此參數則會顯示歐付寶的顯示付款完成頁。 
	 * (有些銀行的WebATM在交易成功後，會停留在銀行的頁面，並不會導回給歐付寶，所以歐付寶也不會將頁面導回到OrderResultURL的頁面。) 
	 * 可為空 
	 * http://www.allpay.com.tw/client.php
	 * @var unknown
	 */
	public static $OrderResultURL = "url/OrderResultURL.php";
	/**
	 * 是否需要額外的付款資訊
	 * Varchar(1)
	 * 設定付款完成通知，及訂單查詢的回覆資料，是否需要額外的付款資訊(回傳資訊請參考-額外回傳的參數)。預設為N，表示不回傳額外資料。若建立訂單時，設定為Y，表示要回傳額外資料。
	 * 可為空
	 * N
	 * @var unknown
	 */
	public static $NeedExtraPaidInfo;
	/**
	 * 裝置來源 
	 * Varchar(10) 
	 * 此參數會因為設定的值不同，而顯示不同layout的付款方式選擇頁面。 
	 * 參數值如下: 
	 * 		P:桌機版頁面(此為預設值)。 
	 * 		M:行動裝置版頁面。
	 * 若是要使用在手機APP付款時，請帶此參數值，且ChoosePayment請帶 ALL給歐付寶
	 * 可為空 
	 * P
	 * @var unknown
	 */
	public static $DeviceSource;
	
	
	/************************************\
	 * 當ChoosePayment參數為使用ATM付款方式時	*
	\************************************/
	
	/**
	 * 允許繳費有效天數 
	 * Int 
	 * 最長 60 天，最短1天。不填則預設為3天。 
	 * 可為空 
	 * 7
	 * @var unknown
	 */
	public static $ExpireDate; 
	/**
	 * Server端回傳付款相關資訊
	 * Varchar(200)
	 * 此網址為訂單建立完成後，歐付寶會將付款相關資訊以Server端方式回傳給特約商店，內容包含繳費銀行代碼、繳費虛擬帳號與繳費期限(yyyy/MM/dd)，但仍然會在歐付寶上顯示繳費的相關資訊。
	 * 可為空
	 * http://www.allpay.com.tw/paymentinfo.php
	 * @var unknown
	 */
	public static $PaymentInfoURL = "url/PaymentInfoURL.php";
	
	/********************************************\
	 * 當ChoosePayment參數為使用CVS或BARCODE付款方式時：	*
	\********************************************/
	
	/**
	 * 交易描述1  交易描述2  交易描述3  交易描述4
	 * Varchar(20) 
	 * 會出現在超商繳費平台螢幕上 
	 * 可為空 
	 * 交易描述1...
	 * @var unknown
	 */
	public static $Desc_1;
	public static $Desc_2;
	public static $Desc_3;
	public static $Desc_4;
	
	/**************************************\
	 * 當ChoosePayment參數為使用Alipay付款方式時：*
	\**************************************/
	
	/**
	 * 商品名稱 
	 * Varchar(200) 
	 * 多個商品請用#號分隔，請先利用UTF-8格式進行 UrlEncode。請已此欄位格式取代原來的
	 * ItemName 
	 * 不可為空
	 * @var unknown
	 */
	public static $AlipayItemName;
	/**
	 * 商品購買數量
	 * Varchar(100)
	 * 多個商品請用#號分隔
	 * 不可為空
	 * @var unknown
	 */
	public static $AlipayItemCounts;
	/**
	 * 商品單價 (非購買商品的小計) 
	 * Varchar(20) 
	 * 多個商品請用#號分隔 
	 * 不可為空
	 * @var unknown
	 */
	public static $AlipayItemPrice;
	/**
	 * 購買人信箱
	 * Varchar(200)
	 * 支付寶紀錄用，本公司不會紀錄
	 * 不可為空
	 * @var unknown
	 */
	public static $Email;
	/**
	 * 購買人電話 
	 * Varchar(20) 
	 * 支付寶紀錄用，本公司不會紀錄 
	 * 不可為空
	 * @var unknown
	 */
	public static $PhoneNo;
	/**
	 * 購買人姓名
	 * Varchar(20)
	 * 支付寶紀錄用，本公司不會紀錄
	 * 不可為空
	 * @var unknown
	 */
	public static $UserName;
	
	/* **************************************\
	 * 當ChoosePayment參數為使用Tenpay付款方式時：	*
	\* **************************************/
	/**
	 * 付款截止時間 
	 * Varchar(20) 
	 * 格式為yyyy/MM/dd HH:mm:ss。只能帶入送出交易後的72小時(三天)之內時間。不填則預設為送出交易後的72小時。
	 * 可為空
	 * @var unknown
	 */
	public static $ExpireTime;
	
	/****************************************\
	 * 當ChoosePayment參數為使用Credit付款方式時：	*
	\* **************************************/
	/**
	 * 刷卡分期期數。 
	 * Int 
	 * 若會員選擇信用卡付款時，商家是否願意提供分期，如果願意，請帶可分期期數。
	 * 如不提供分期，請帶0。 
	 * 可為空
	 * @var unknown
	 */
	public static $CreditInstallment;
	/**
	 * 使用刷卡分期的付款金額。
	 * Money
	 * 如果使用刷卡分期的消費金額會大於不分期的消費金額時，請帶使用分期的消費金額。若不使用信用卡分期時，請帶0
	 * 可為空
	 * @var unknown
	 */
	public static $InstallmentAmount;
	
	/**
	 * 信用卡是否使用紅利折抵。 
	 * Varchar(1) 
	 * 設為Y時，當歐付寶會員選擇信用卡付款時，會進入紅利折抵的交易流程。 
	 * 可為空
	 * @var unknown
	 */
	public static $Redeem;
	/**
	 * 是否為銀聯卡交易
	 * Int
	 * 是否為銀聯卡。否-請帶0(預設值)，是-請帶1。當此參數帶1時，表示此筆交易為銀聯卡交易，歐付寶會直接將頁面導到銀聯網站。(銀聯卡交易必須跟歐付寶提出申請，方可使用)
	 * 可為空
	 * @var unknown
	 */
	public static $UnionPay;
	
	/**
	 * 初始參數
	 * @param unknown $options
	 */
	public static function init($options=null){
		if($options != null){
			Allpay::$MerchantID = $options["MerchantID"];
			Allpay::$ReturnURL = $options["ReturnURL"];
			Allpay::$HashKey = $options["HashKey"];
			Allpay::$HashIV = $options["HashIV"];
			Allpay::$ChoosePayment = $options["ChoosePayment"]; // 目前強制要使用 	 * CVS:超商代碼與 BARCODE:超商條碼
			
			if(array_key_exists("TradeDesc", $options))
				Allpay::$TradeDesc = $options["TradeDesc"];
			
		}
	}
	
	/**
	 * 在與歐付寶進行資料傳遞時，除了CheckMacValue參數外，其餘所有參數皆需要加入檢查碼的檢核機制。
	 * 檢碼核制如下:
	 * 將傳遞參數依照第一個英文字母，由A到Z的順序來排序(遇到第一個英名字母相同時，以第二個英名字母來比較，以此類推)，
	 * 並且以&方式將所有參數串連後，再將參數最前面加上HashKey、最後面加上HashIV，並將整串字串進行URL encode後再轉為小寫，
	 * 再以MD5加密方式來產生CheckMacValue。
	 * @param unknown $array
	 * @return string
	 */
	public static function getCheckMacValue($array){
		
		$key_array = array();
		foreach ($array as $key => $value) {
			$key_array[] = $key;
		}
		// 排序參數
		sort($key_array);
		
		// 組合餐數
		$str = "HashKey=".Allpay::$HashKey;
		foreach ($key_array as $key) {
			$value = urldecode($array[$key]);
			$str .= "&$key=$value";
		}
		$str .= "&HashIV=".Allpay::$HashIV;
		
// 		echo $str;
		
		// url encode
		$str = urlencode($str);
		
		// 轉乘小寫
		$str = strtolower($str);
		
		// MD5 
		$str = md5($str);
		
		Allpay::$CheckMacValue = $str; 
		
		return $str;
	}	
}