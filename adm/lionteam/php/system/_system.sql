-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2014 at 03:47 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `youlong`
--

-- --------------------------------------------------------

--
-- Table structure for table `_system`
--

CREATE TABLE IF NOT EXISTS `_system` (
  `sn` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `title` varchar(60) NOT NULL COMMENT '前台title',
  `email` varchar(60) NOT NULL COMMENT '系統信箱',
  `name_for_mail` varchar(20) NOT NULL COMMENT '發信人名稱',
  `meta_description` varchar(255) NOT NULL COMMENT 'meta description',
  `meta_keyword` varchar(255) NOT NULL COMMENT '前台關鍵字',
  `facebook_add_id` varchar(60) NOT NULL COMMENT 'facebook 登入api_id',
  `facebook_app_secret` varchar(60) NOT NULL COMMENT 'facebook 登入api_secret',
  `allpay_client_id` varchar(60) NOT NULL COMMENT '歐付寶 client_id',
  `allpay_hashkey` varchar(60) NOT NULL COMMENT '歐付寶 hashkey',
  `allpay_hashiv` varchar(60) NOT NULL COMMENT '歐付寶 hashiv',
  `ec_remark` text NOT NULL COMMENT '購物車說明頁',
  PRIMARY KEY (`sn`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='系統設定欄位' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `_system`
--

INSERT INTO `_system` (`sn`, `title`, `email`, `name_for_mail`, `meta_description`, `meta_keyword`, `facebook_add_id`, `facebook_app_secret`, `allpay_client_id`, `allpay_hashkey`, `allpay_hashiv`, `ec_remark`) VALUES
(1, '', 'servcie@yolon.com', '', '', '', '', '', '', '', '', '');
