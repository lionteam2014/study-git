<?php

/**
 * @author raybird
 *
 */
class Session {
	
	public static function init(){
		if( !file_exists("session_data")){
			mkdir("session_data"); // default mode 0777
		}
		
		session_save_path("session_data");
		session_start ();
		ini_set ( 'session.name', 'pissession' ); // session值使用名稱
		ini_set ( 'session.gc_probability', 1 ); // 回收處理分子項
		ini_set ( 'session.gc_divisor', 1 ); // 回收處理分母項
		ini_set ( 'session.cookie_lifetime', 3600 ); // Cookie的SID存在時間
// 		session_start();
	}
	
	public static function set($key, $value){
		$_SESSION[$key] = $value;
	}
	
	public static function get($key){
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}else{
			return null;
		}
	}
	
	public static function destroy(){
		// Unset all of the session variables.
		$_SESSION = array();
		
		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
			);
		}
		
		// Finally, destroy the session.
		session_destroy();
	}
}