<?php

/**
 * @author raybird
 *
 */
class Product extends \Controller {
	
	/*
	 * (non-PHPdoc) @see Controller::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	/*
	 * (non-PHPdoc) @see Controller::after()
	 */
	public function after() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::destory()
	 */
	public function destory() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::before()
	 */
	public function before() {
		// TODO Auto-generated method stub
	}
	
	public function index(){
		Loader::load("lionteam/php/product/ProductView.php");
		$view = new ProductView();
		$view->render("lionteam/php/product/views/list.php");
	}
	
}