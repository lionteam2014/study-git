<?php
/**
 * 國泰世華信用卡
 * 感謝
 * 國泰世華銀行 信用卡暨支付金融部 收單科(聽聲音正妹)
 * 徐偲恬 Stella Hsu
 * 電　話：02-2383-0111分機1301
 * 地   址：108台北市萬華區中華路1段88號11F
 * E-mail：stellahsu@cathaybk.com.tw
 * 若是無分期則不需要帶入 MSGID 與 PERIODNUMBER
 * 
 * 需與國泰世華設定頁面。
 * 
 * @author raybird
 *
 */
class Creditcard {

	/** 分期商店代號 */
	var $STORE_ID ;
	/** 商店CA資料 */
	var $CUBKEY ;
	/** 國泰世華訂單初始頁面 */
	var $CATHAY_ORDER_INITIAL = "https://sslpayment.uwccb.com.tw/EPOSService/Payment/OrderInitial.aspx";	
	/** 信用卡單號 */
	var $order_number;
	/** 信用卡分期 */
	var $period_number;
	/** 刷卡金額 */
	var $amount;
	/** 刷卡成功後頁面 */
	var $success_url;
	/** 刷卡失敗頁面 */
	var $error_url;
	
	public function __construct($option){
		$this->STORE_ID = $option['store_id'];
		$this->CUBKEY = $option['cubkey'];
	}
	
	/**
	 * 取得訂單所需要的xml
	 * @param unknown $order_number
	 * @param unknown $period_number
	 * @param unknown $amount
	 * @return NULL|string
	 */
	public function get_xml($order_number,$period_number,$amount){
		$_order_number_status = $this->_check_order_number($order_number);
		if(!$_order_number_status){
			echo '訂單非 A-Z0-9';
			return NULL; // 訂單非 A-Z0-9
		}
		
		if($period_number==1){
			$cavalue = md5($this->STORE_ID.$order_number.$amount.$this->CUBKEY);
			$strRqXML = "<?xml version='1.0' encoding='UTF-8'?>
						<MERCHANTXML>
							<CAVALUE>$cavalue</CAVALUE>
							<ORDERINFO>
								<STOREID>".$this->STORE_ID."</STOREID>
								<ORDERNUMBER>$order_number</ORDERNUMBER>
								<AMOUNT>$amount</AMOUNT>
							</ORDERINFO>
						</MERCHANTXML>";
		}else{
			$cavalue = md5($this->STORE_ID.$order_number.$amount.$period_number.$this->CUBKEY);	
			$strRqXML = "<?xml version='1.0' encoding='UTF-8'?>
						<MERCHANTXML>
							<CAVALUE>$cavalue</CAVALUE>
							<MSGID>TRS0002</MSGID>
							<ORDERINFO>
								<STOREID>".$this->STORE_ID."</STOREID>
								<ORDERNUMBER>$order_number</ORDERNUMBER>
								<AMOUNT>$amount</AMOUNT>
								<PERIODNUMBER>$period_number</PERIODNUMBER>
							</ORDERINFO>
						</MERCHANTXML>";
		}
		return $strRqXML;
	}
	
	/**
	 * 檢查訂單編號規則最大20字，A-Z0-9
	 * @param unknown $order_number
	 * @return boolean
	 */
	private function _check_order_number($order_number){
		if($order_number.length<=20 && preg_match("([A-Z0-9]{0,20})", $order_number))
			return TRUE;
		else
			return FALSE;
	}
	
	/**
	 * 取得刷卡表單。
	 * 呼叫 get_xml 後所使用
	 * @param unknown $xml
	 * @param string $auto_send 是否在頁面送出 form 的 javascript
	 */
	public function get_form($xml,$auto_send=TRUE){

		$_form = '<form name="creditcard" id="creditcard" action="'.$this->CATHAY_ORDER_INITIAL.'" method="post" >
					<input type=hidden name="strRqXML" value="'.$xml.'">
				  </form>';
		if($auto_send)
			$_form .='<script>document.getElementById("creditcard").submit();</script>';
		return $_form;
	}

	
	/**
	 * 接收銀行 xml 轉換成 obj
	 * @param unknown $xml
	 * @return SimpleXMLElement
	 */
	public static function xmltoobj($xml){
		$data = $xml;
		
		// 去除跳脫字元的反斜線
		$data = str_replace("\'","'",$data);
		$data = str_replace("\"","'",$data);
		// 轉換成 xml 物件
		$xmlObj = simplexml_load_string($data);

		return $xmlObj;
	}
	
	/**
	 * 確認訂單成功
	 * @param unknown $xml
	 * @return boolean 成功回傳 true
	 */
	public function get_confirm($xml){
		$xmlObj = $this->xmltoobj($xml);
		
		$STOREID = (string)$xmlObj->ORDERINFO->STOREID;
		$ORDERNUMBER = (string)$xmlObj->ORDERINFO->ORDERNUMBER;
		$AMOUNT = (string)$xmlObj->ORDERINFO->AMOUNT;
		$AUTHSTATUS = (string)$xmlObj->AUTHINFO->AUTHSTATUS;
		$CAVALUE = (string)$xmlObj->ORDERINFO->CAVALUE;
		// 商店正確且刷卡成功，則更新資料庫payState
		if($STOREID==$this->STORE_ID && $AUTHSTATUS=="0000"){
			// 資料庫確定有此筆資料，將此比交易設定為成功
			return true;
		}else{
			return false;
		}
	}
	
	
	public function go_result($domain,$page){
		//MD5演算法加密(RETURL網域名稱(例如www.cathaybk.com.tw)+CUBKEY)
		$returl = "http://".$domain."/".$page;
		$cavalue =md5($domain.$this->CUBKEY);
		$response="<?xml version='1.0' encoding='UTF-8'?>
		<MERCHANTXML>
		<CAVALUE>$cavalue</CAVALUE>
		<RETURL>$returl</RETURL>
		</MERCHANTXML>";
		return $response;
	}
}
?>