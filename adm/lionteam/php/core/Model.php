<?php
/**
 * @author raybird
 *
 */
class Model{
	
	public function __construct(){
		DB::getInstance();
	}
	
	/**
	 * 取得PDO物件
	 * @return PDO
	 */
	public static function getPDO(){
// 		$dsn 		= 'mysql:dbname='.Config::$DBNAME.';host='.Config::$DBHOST;
// 		$username 	= Config::$DBUSER;
// 		$passwd 	= Config::$DBPASS;
// 		$options 	= array(
// 				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
// 				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
// 		);
// 		try {
// 			$db = new PDO($dsn, $username, $passwd, $options);
// 		} catch (PDOException $e) {
// 			echo 'Connection failed: ' . $e->getMessage();
// 			die();
// 		}
		
		return DB::getInstance();
	}
	
	public function test(){
		$this->db = Model::getPDO();
		$_sth = $this->db->query("show tables");
		$_sth->execute();
		$data = $_sth->fetch(PDO::FETCH_OBJ);
		var_dump($data);
	}
}