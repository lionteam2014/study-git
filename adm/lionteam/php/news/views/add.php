<div class="row">

	<div class="col-sm-12">
		<div class="pull-left">
			<button class="btn btn-success js-load" data-toggle="tooltip" data-placement="bottom" title="回列表" data-url="news/news"><?=html_space(3)?><span class="glyphicon glyphicon-arrow-left"></span><?=html_space(3)?></button>
		</div>
	
		<div class="pull-right">
			<button class="btn btn-primary js-load" data-toggle="tooltip" data-placement="bottom" title="重新整理" data-url="news/news/add"><span class="glyphicon glyphicon-refresh"></span> </button>
		</div>
	
		<div class="text-center"><h4>新增最新消息<small></small></h4></div>
		
		
		<form id="data" class="form-horizontal" role="form" onsubmit="return false" enctype="multipart/form-data">
			
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">狀態</label>
				<div class="col-sm-8">
					<label class="radio-inline">
  						<input type="radio" id="status" name="status" value="1" checked> 顯示
					</label>
					<label class="radio-inline">
  						<input type="radio" id="" name="status" value="0"> 不顯示
					</label>
				</div>
			</div>
			
			<div class="form-group">
				<label for="subject" class="col-sm-2 control-label">主題</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="subject" name="subject" placeholder="主題" required>
				</div>
			</div>
			<br />
			<div class="row">
				<label for="answer" class="col-sm-12 control-label"><p class="text-center">內容</p></label>
				<div class="col-sm-12">
					<textarea id="content" name="content" cols="30" rows="10" ></textarea>
				</div>
			</div>
			<br />
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
      				<button type="submit" id="send" class="btn btn-success pull-right" data-toggle="tooltip" data-placement="bottom" title="送出"><?=html_space(3)?><span class="glyphicon glyphicon-save"><?=html_space(1)?></button>
    			</div>
  			</div>
		</form>
	
	</div>

</div>
<script>
(function(){

	var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
	editorHeight = h*0.4;
	
	var editor = CKEDITOR.replace( 'content',{
		height: editorHeight+'px',

	} );	

	CKFinder.setupCKEditor( editor, 'editor/ckfinder/' );

})();
</script>

<script>

(function(){

	$('.btn').tooltip();
	// hashAction.js 設定
	$(".js-load").loadPage('',$('#page-wrapper'));

})();

</script>

<script>
$(document).ready(function(){
	$("#send").on('click', function(){
// 		console.log('click');
		$('#data').validate({
		    rules: {
		    	subject: {
		    		minlength: 1,
		            required: true,
		        },
		        
		    },
		    messages: {
				/*需驗證的欄位的錯誤訊息*/
				subject: "請輸入主題",
				
			},
		    submitHandler: function (form){
		    	blockUI("新增中");
		    	
		    	var options = {
						 url: 'news/news/add_action',
						 cache: false,
						 dataType: 'json',
						 type:'POST',
						 success: function(response) {
							$.unblockUI();
							if(response.result) {
								bootbox.alert('新增成功!', function(){
									
									location.hash = "news/news";
								});
							}else{
								bootbox.alert('新增失敗!'+response.message, function(){
									
								});
							}
						}
				}; 
				$("#data").ajaxSubmit(options); 
                return false; 
		    }
		});
	});
});
</script>