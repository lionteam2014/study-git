<div class="row">
	<div class="col-sm-12">

		<button class="btn btn-primary js-load" id="add" data-toggle="tooltip" data-placement="bottom" title="新增" data-url="product/product/add"'><?=html_space(3)?><span class="glyphicon glyphicon-plus"></span><?=html_space(3)?></button>
		<?=html_space(4)?>
		
		<label>
			<input type="checkbox" id="checkbox-all" class="checkbox-css form-inline" name="checkbox-all"/><strong>&nbsp;&nbsp;全選</strong>
		</label>
		
		<?=html_space(4)?>
		
		<div class="btn-group">
			<button class="btn btn-danger btn-action checkbox-function hidden inline" id="delete" data-toggle="tooltip" data-placement="bottom" title="刪除"><?=html_space(4)?><span class="glyphicon glyphicon-trash"></span><?=html_space(4)?></button>
			<button class="btn btn-primary btn-action checkbox-function hidden inline" id="enable" data-toggle="tooltip" data-placement="bottom" title="顯示"><?=html_space(4)?><span class="glyphicon glyphicon-ok"></span><?=html_space(4)?></button>
			<button class="btn btn-primary btn-action checkbox-function hidden inline" id="disable" data-toggle="tooltip" data-placement="bottom" title="不顯示"><?=html_space(4)?><span class="glyphicon glyphicon-remove"></span><?=html_space(4)?></button>
		</div>
		
		<button class="btn btn-primary js-load pull-right" data-toggle="tooltip" data-placement="bottom" title="重新整理" data-url="product/product"><span class="glyphicon glyphicon-refresh"></span><?=html_space(1)?></button>

	</div>
</div>

<br />
<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
		<form id='data' onsubmit="return false" enctype="multipart/form-data">
			<table class="table table-hover">
				<thead>
					<tr class='success'>
						<td width="60%" align="left"><input type="checkbox" id="checkbox-all" class="checkbox-css" name="checkbox-all"/><strong>&nbsp;&nbsp;主題</strong></td>
						<td width="20%" align="center"><strong>狀態</strong></td>
						<td width="20%" align="center"><strong>功能</strong></td>
					</tr>
				</thead>
				<tbody class="sortable">
<?php 
	if(count($data) == 0){
?>
		<td align="center" colspan="3"><strong class="text-danger">目前尚無項目 !</strong></td>
<?
	}else{

		foreach ($data as $obj) {
			$status_string = "顯示";
			$status_class = "text-primary";
			if(($obj->status == '0')){
				$status_string = "不顯示";
				$status_class = "text-danger";
			}
?>			
					<tr id="<?=$obj->sn?>" style="cursor:move;">
						<td align="left" >
							<input type="checkbox" id="<?=$obj->sn?>" name="checkbox-items[]" value="<?=$obj->sn?>" class='checkbox-item checkbox-css'/><label for="<?=$obj->sn?>">&nbsp;&nbsp;</label> 
							<span class="tooltip-item" data-toggle="tooltip" data-placement="bottom" title="檢視內容">
								<a href="javascript:;" class="text-info show-bootbox" data-value="<?=$obj->sn?>"><span class="glyphicon glyphicon-eye-open"></span> <?=$obj->question?></a>
							</span>
						</td>
						<td align="center"><span class="<?=$status_class?>"> <?=$status_string?></span></td>
						<td align="center">
							<button class="btn btn-primary js-load" id="modify" data-toggle="tooltip" data-placement="bottom" title="修改" data-url="news/news/modify?sn=<?=$obj->sn?>" ><?=html_space(1)?><span class="glyphicon glyphicon-cog"></span><?=html_space(1)?></button>
						</td>
					
					</tr>
<?php 
		}
	}
?>			
				</tbody>
			</table>
		</form>
		</div>
	
	</div>
</div>

<script>

(function(){

	$('.btn').tooltip();
	// hashAction.js 設定
	$(".js-load").loadPage('',$('#page-wrapper'));

})();

</script>
