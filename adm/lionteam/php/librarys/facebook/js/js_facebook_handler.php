<?php
include_once 'facebook.php';
/**
* 這用來中介處理 json 物件，在此透過 index_add_member.php 把必須純入資料庫資訊紀錄。
* 如果 php 對 json 處理熟悉的話可以自行 override FacebookService 的 facebookJsonHandler() 
*/
$json = $jsFBService->getFacebookUserJson($_GET['code']);
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

$(document).ready(function(){

var json = <?=$json?>; // php 處理facebook登入後取得的json物件

var check = function(){
	var data = {
			"account"	:json.email,
			"pass" 		: "",
			"email"		: json.email,
			"name"		: json.name	,
			"sex"		: json.gender,
			"fb_id"		: json.id
			};
	$.ajax({
		  type:'post',
		  url:'ajax_function/index_add_member.php', // 此 php 進行後端會員資料紀錄
		  data:data,
		  dataType:"json", 
		  cache:false,				 
		  success: function(receiveData) {
		   		if(receiveData.check=="ok"){
		   			alert("歡迎 "+json.name+" 登入!");
			 	}else{	
				 	alert("歡迎 "+json.name+" 再次回來!");
			 	}
			 	//
			 	location.href="<?='http://'.$_SERVER['HTTP_HOST'].$_GET['state']?>"; 	
		  	}	 
		}); //ajax 結束
	}; // check 結束
	
	check();
});
</script>