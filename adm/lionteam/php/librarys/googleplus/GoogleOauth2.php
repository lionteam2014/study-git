<?php

/**
 * @author raybird
 *
 */
class GoogleOauth2 {
	
	const response_type_token = "token";
	
	const response_type_code = "code";
	
	static $client_id;
	
	static $client_secret;
	
	/**
	 * 初始Google+ client_id
	 * @param unknown $options
	 */
	public static function init($options){
		GoogleOauth2::$client_id = $options["client_id"];
		GoogleOauth2::$client_secret = $options["client_secret"];
	}
	
	/**
	 * 取得 Google+ 登入服務。
	 * 
	 * 參考來源:https://developers.google.com/accounts/docs/OAuth2WebServer?hl=zh-TW
	 * 
	 * @param unknown $redirect_uri 登入成功後要回導網址。
	 * @param $response_type 回傳參數的形式。
	 * GoogleOauth2::response_type_token 會將回應放在 url#...則需搭配 callbackHandlerJs 利用 js 解析。
	 * @throws Exception
	 * @return string 登入的網址
	 */
	public static function getLoginLink($redirect_uri, $response_type=GoogleOauth2::response_type_code){
		if(GoogleOauth2::$client_id==null){
			throw new Exception("GoogleOauth2 should be init first.");
		}
		 
		return "https://accounts.google.com/o/oauth2/auth?scope=email+profile&state=/profile&redirect_uri=$redirect_uri&response_type=$response_type&client_id=".GoogleOauth2::$client_id;
	}
	
	/**
	 * 用 server php 端去透過 CURL 的方式去取得資料
	 * @param unknown $handler_url
	 * @return string
	 */
	public static function callbackHandlerPhp($handler_url){
		// ?state=/profile&code=4/jVpZhFeVVu2nfrXyQlpGR-M6XvC6.Im1OyNAcoDMTgrKXntQAax1GCsNsiAI
		if(array_key_exists("code", $_GET)){
			
			$post_arry = array(
					"code"=> $_GET["code"],
					"client_id"=>GoogleOauth2::$client_id,
					"client_secret"=>GoogleOauth2::$client_secret,
					"redirect_uri"=>$handler_url,
					"grant_type"=>"authorization_code",
			);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://accounts.google.com/o/oauth2/token");
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arry);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			$result = curl_exec($ch);
			curl_close ($ch);

			$response = json_decode($result);

			$access_token = $response->access_token;
			$graph_url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$access_token;
			// 取得使用者資訊
			return file_get_contents($graph_url);
		}
	}
	
	/**
	 * 頁面 js 處理動作。不好判斷因為google 轉導的值掛在 url 的 hash 上，並不會由 php 取的
	 * 這部份得用 js 在 browser 端處裡
	 * @param string $needJQuery
	 * @return string
	 */
	public static function callbackHandlerJs($handler_url){
		echo $redirect_uri;
		$js = <<<eof
		<script>
		function getParams(){
			var params = {}, queryString = location.hash.substring(1),
		    regex = /([^&=]+)=([^&]*)/g, m;
			
			while (m = regex.exec(queryString)) {
			  params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
			}
			return params;
		}

		function getJson(){
			var params= getParams();
			
			if("access_token" in params){
				location.href="$handler_url?access_token="+params['access_token'];
			}
		}
		
		(function(){
			getJson();
	    })();
		</script>			
eof;
		return $js;
	}
	
	/**
	 * 顯示登入後可取得資訊
	 * @return string|NULL
	 */
	public static function getGoogleJson(){
		if(array_key_exists("access_token", $_GET)){
			$graph_url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$_GET['access_token'];
			// 取得使用者資訊
			return file_get_contents($graph_url);
		}
		return null;
	}
	
	
}