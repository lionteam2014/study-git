-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2014 at 03:48 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `youlong`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `sn` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `status` int(2) NOT NULL COMMENT '顯示狀態',
  `content` text NOT NULL COMMENT '內容',
  PRIMARY KEY (`sn`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='關於我們' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`sn`, `status`, `content`) VALUES
(1, 1, '<table style=\\"width:100% ; color: white;\\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\\"vertical-align:middle\\">\r\n			<h2>Familyfishes 一家好魚</h2>\r\n			</td>\r\n			<td style=\\"vertical-align:middle\\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\\"vertical-align:middle\\">&nbsp;</td>\r\n			<td style=\\"vertical-align:middle\\">\r\n			<p><br />\r\n			關於好魚品牌介紹:<br />\r\n			帶著五星級高檔食材輕鬆到家的理念～我們終於在2014年成立了！</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>對於品質的堅持：</strong><br />\r\n			從小在雲林箔子寮漁港長大的張老闆，從小接觸各類海產、熱愛各式海鮮！<br />\r\n			更對海鮮的原味有著獨到的追求與堅持！現在更帶著10多年的實體販售經驗跨足至網路市場！</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>對於服務的熱誠：</strong><br />\r\n			對產品控管以及通路販售有著多年豐富經驗的王老闆，在其帶領的工作團隊運作下，<br />\r\n			對於各項流程的細心運作，絕對帶給每位顧客好品質的服務。</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p>我們提供:</p>\r\n\r\n			<p>&quot;新鮮&quot;&nbsp; 品質看得見 ! 商品個個皆是主打～</p>\r\n\r\n			<p>&quot;美味&quot;&nbsp; 嚴選再嚴選 ! 食材道道皆是細心～</p>\r\n\r\n			<p>&quot;便利&quot;&nbsp; 訂購超簡單 ! 貨物天天快速配送～</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n');
