<?php
/**
 * 1. 送出表單頁面
 */

/*
210050011的URL已設定為http://mashup.com.tw/confirm.php

 

測試用商店資料

商店代號: 010990043

CUBKEY: 942a73016ec76819dbdc56312c79f7f4

測試期限至 2013/11/22

*/
require_once 'Creditcard.php';

// 假訂單定單內容
$order_number = $_POST['order_number']; // 每次訂單都要不一樣，檢查訂單編號規則最大20字，A-Z0-9
$period_number = $_POST['period_number']; // 分期數 1,3,6,12 
$amount = $_POST['amount']; // 金額至少3元
$STOREID = $_POST['store_id'];
$CUBKEY = $_POST['cubkey'];
$option = array();

if($period_number==1){ // 未分期

	$option = array();
	switch ($STOREID){
		case '210050011':
			$option = array (
				'store_id' => '210050011',
				'cubkey' => 'adbc35a8be097f28f0c89f03733e5f06'
				);
			break;
		case '010990043':
			$option = array(
				'store_id' => '010990043',
				'cubkey' => '942a73016ec76819dbdc56312c79f7f4'
				);
			break;
	}

}else{
	$option = array(
			'store_id' => '210050013',
			'cubkey' => 'a880e91a5e11c0ead2d70738217fe2b1'
	);
}	

$option= array('store_id' => $_POST['store_id'] ,
			   'cubkey'   => $_POST['cubkey'] );
			   
$creditor = new Creditcard($option);

/**
 * 取得訂單所需要的xml
 * @param unknown $order_number 訂單編號 20 碼內，英文數字
 * @param unknown $period_number 分期期數
 * @param unknown $amount 金額
 * @return NULL|string 回傳 xml 格式
 */
$xml = $creditor->get_xml($order_number, $period_number, $amount);

$auto_send=true;
/**
 * 取得刷卡表單，要將刷卡資訊寫入資料庫以便 confirm.php 做查詢比對
 * @param unknown $xml
 * @param string $auto_send
 */
$form = $creditor->get_form($xml,$auto_send);
echo $form;

