-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2014 at 03:50 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `youlong`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `sn` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `subject` varchar(60) NOT NULL COMMENT '主題',
  `content` text NOT NULL COMMENT '內容',
  `status` int(2) NOT NULL COMMENT '顯示狀態',
  `post_time` datetime NOT NULL COMMENT '發布時間',
  `sort` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`sn`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='最新消息' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`sn`, `subject`, `content`, `status`, `post_time`, `sort`) VALUES
(3, 'retretete123', '<p>rteertetsacscasc<img alt=\\"\\" src=\\"editor/ckfinder/userfiles/images/Jumpman-Logo.jpg\\" style=\\"height:82px; width:88px\\" /></p>\r\n\r\n<p>scascsa</p>\r\n', 1, '2014-05-03 23:18:56', 2),
(2, 'test2', '<p>cscdcsc<img alt=\\"\\" src=\\"editor/ckfinder/userfiles/images/Jumpman-Logo.jpg\\" style=\\"height:60px; width:64px\\" />cdcdcdcsc</p>\r\n\r\n<p>edcdscsdcdc</p>\r\n', 1, '2014-05-03 22:48:28', 4),
(4, '23', '', 1, '2014-05-10 21:32:35', 3),
(5, 'test', '<p>test</p>\r\n', 1, '2014-05-10 22:58:24', 1);