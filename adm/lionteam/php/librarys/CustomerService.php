<?php

/**
 * for 專案客製用
 * @author raybird
 *
 */
class CustomerService {
	
	private static $function_menus;
	
	public static function getMenuArray(){
		
		if(CustomerService::$function_menus == null){
			
			CustomerService::$function_menus = array();
			
			$sql = "select * from _function_menu where status=1 order by sort,sn desc";
			$stmt = DB::prepare($sql);	
			$result = $stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_OBJ);
			
			foreach ($data as $obj) {
				CustomerService::$function_menus[$obj->name] = array("url" =>$obj->url,"img"=>$obj->file,"sn"=>$obj->sn);
			}
			

		}
		return CustomerService::$function_menus;
	}
	
	public static function getBreadcrumb($function_key){
		$time = date(DateTime::ISO8601);
		$string = <<<EOF
		<div class="row">
			<div class="col-sm-12">
				<ol class="breadcrumb">
					<li class="js-load" data-url="main.php?api=index/index"><a href="javascript:;">功能選單</a></li>
					<li class="active">$function_key</li>
				</ol>
			</div>
		</div>
EOF;
		return $string;
	}
}