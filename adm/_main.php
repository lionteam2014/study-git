<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="shortcut icon" href="images/fish.ico" type="image/x-icon">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>後台管理</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/animate.css" rel="stylesheet">
<link href="css/customer-animate.css" rel="stylesheet">
<link href="css/starter-template.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">lionteam/php</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="index.html">首頁</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>

	<div class="container">

		<div class="row">
		
			<div class="col-sm-offset-4 col-sm-4">
				<div class="panel panel-success bounceIn">
					<div class="panel-body">
						<form class="form-horizontal" role="form" action="user/login/in" method="post">
							<input type="hidden" name="check_login" value="ok" />
							
							<h2 class="form-signin-heading text-center">管理者後台</h2>
							
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-3 control-label">帳號</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="account" name="account" placeholder="請填寫帳號" required="required">
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-3 control-label">密碼</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password" name="password" placeholder="請填寫密碼" required="required">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-success">登入</button>
								</div>
							</div>
		
						</form>
					</div>
				</div>

			</div>
		</div>

		<div class="starter-template"></div>

	</div>
	<!-- /.container -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.js"></script>
</body>
</html>