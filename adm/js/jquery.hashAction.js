/**
 * 使用 ajax load page
 * 可帶參數
 * Get 參數
 * data-url=main.php?test=123&args=1
 * @deprecated data-post='{"id":"3","group":"a"}'
 * @deprecated options 用 json 可以帶 post 參數
 * @author raybird
 */
$(function($, window) {

	$(window).on('hashchange', function () { //detect hash change
//		var base = "youlong/adm/";
		var hash = window.location.hash.slice(1); //hash to string (= "myanchor")
		console.log('hashchange='+hash);
		//do sth here, hell yeah!
		$('#page-wrapper').load(hash);

	});

	/**
	 * @param actionPage 要載入資訊
	 * @param targetView 要載入 html 結果的 jQuery 物件
	 * @param 載入完畢呼叫的 callback
	 */
	$.fn.loadPage = function (actionPage, targetView, callback){
		var $that = $(this);

		function fireCallback(responseText, textStatus, XMLHttpRequest){
			if (typeof callback == 'function') { // make sure the callback is a function
		        callback.call(this); // brings the scope to the callback
		    }
		}		
		
		$.each(this,function() {

			var $this = $(this);
			
			// 避免重複加入事件綁定
			if($this.hasClass("in-loadPage"))
				return;
			else
				$this.addClass("in-loadPage");
			
			var token,
			// 判斷 target 物件是否為 jquery 物件
			$targetView = (typeof targetView === 'string')?$("#"+targetView):targetView,
			urlPath = $this.attr('data-url'),
			url = actionPage+urlPath;
			
			$this
			.on('click', function(e){
				e.preventDefault();
				$targetView.html("");
				token = new Date().getTime();
				location.hash = url+"?token="+token;
				fireCallback();
			});
		});
		return $that;
	};
}(jQuery, window));