<?php
/**
 * 設定檔相關
 * @author raybird
 *
 */
class Config{
	
	public static $DBTYPE 	= "mysql";
	public static $DBHOST 	= "localhost";
	public static $DBUSER 	= "";
	public static $DBPASS 	= "";
	public static $DBNAME 	= "";
	
}