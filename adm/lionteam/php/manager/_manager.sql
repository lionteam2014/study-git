-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2014 at 03:46 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `youlong`
--

-- --------------------------------------------------------

--
-- Table structure for table `_manager`
--

CREATE TABLE IF NOT EXISTS `_manager` (
  `sn` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `account` varchar(20) NOT NULL COMMENT '帳號',
  `password` varchar(32) NOT NULL COMMENT '密碼',
  `email` varchar(60) NOT NULL COMMENT '信箱',
  `remark` varchar(255) NOT NULL COMMENT '備註',
  `name` varchar(20) NOT NULL COMMENT '名稱',
  `login_times` int(11) NOT NULL COMMENT '登入次數',
  `super` int(1) NOT NULL COMMENT '超級帳號(1),一般(0)',
  `status` int(2) NOT NULL COMMENT '狀態(1正常，0不可登入)',
  PRIMARY KEY (`sn`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='帳號管理' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `_manager`
--

INSERT INTO `_manager` (`sn`, `account`, `password`, `email`, `remark`, `name`, `login_times`, `super`, `status`) VALUES
(1, 'raybird', 'raybird', 'a691228@gmail.com', '', 'test', 63, 1, 1),
(2, 'test', 'test', 'test@cc.cc', '', 'test', 65, 0, 1),
(3, 'fishroot', 'root', 'kevin@test.com', 'test', '店家', 0, 0, 1),
(4, '1', '2', '3@ff.cc', '5', '4', 0, 0, 0);