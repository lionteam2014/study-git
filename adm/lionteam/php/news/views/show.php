<div class="row">

	<div class="col-sm-12">
		<div class="pull-left">
			<button class="btn btn-success js-load" data-toggle="tooltip" data-placement="bottom" title="回列表" data-url="news/news"><?=html_space(3)?><span class="glyphicon glyphicon-arrow-left"></span><?=html_space(3)?></button>
		</div>
	
		<div class="pull-right">
			<button class="btn btn-primary js-load" data-toggle="tooltip" data-placement="bottom" title="重新整理" data-url="news/news/show?sn=<?=$data->sn?>"><span class="glyphicon glyphicon-refresh"></span> </button>
		</div>
	
		<div class="text-center"><h4>最新消息<small></small></h4></div>
		
		
		<form id="data" class="form-horizontal well" role="form" onsubmit="return false" enctype="multipart/form-data">
			
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">狀態</label>
				<div class="col-sm-8">
					<p class="form-control-static"><?=($data->status=='1')?'顯示':'不顯示'?></p>
				</div>
			</div>
			
			<div class="form-group">
				<label for="subject" class="col-sm-2 control-label">主題</label>
				<div class="col-sm-8">
					<p class="form-control-static"><?=$data->subject?></p>
				</div>
			</div>
			
			<div class="form-group">
				<label for="subject" class="col-sm-2 control-label">發布時間</label>
				<div class="col-sm-8">
					<p class="form-control-static"><?=$data->post_time?></p>
				</div>
			</div>
			
			<br />
			<div class="row">
				<label for="answer" class="col-sm-12 control-label"><p class="text-center">內容</p></label>
				<div class="col-sm-offset-1 col-sm-10 ">
					<div class="panel panel-default">
						<div class="panel-body">
							<?=stripslashes($data->content)?>
						</div>
					</div>
				</div>
			</div>
			<br />

		</form>
	
	</div>

</div>

<script>

(function(){

	$('.btn').tooltip();
	// hashAction.js 設定
	$(".js-load").loadPage('',$('#page-wrapper'));

})();

</script>
