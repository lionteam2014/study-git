<?php

/**
 * @author raybird
 *
 */
class Manager extends \Controller {
	
	/*
	 * (non-PHPdoc) @see Controller::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	/*
	 * (non-PHPdoc) @see Controller::after()
	 */
	public function after() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::destory()
	 */
	public function destory() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::before()
	 */
	public function before() {
		// TODO Auto-generated method stub
	}
	
	public function index(){
		Loader::load("lionteam/php/manager/ManagerView.php");
		$view = new ManagerView();
		$view->render("lionteam/php/manager/views/list.php");
	}
	
	public function login(){
		Loader::load("lionteam/php/manager/ManagerModel.php");
		$model = new ManagerModel();
		
		$result = $model->login();
		
		$this->responseSimpleJson($result);
	}
	
	public function logout(){
		Session::destroy();
		header("Location: ../../");
	}
}