<?php

/**
 * @author raybird
 *
 */
class AboutModel extends \Model {
	
	/*
	 * (non-PHPdoc) @see Model::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	public function getAbout(){
		$pdo = DB::getInstance();
		$sql = "select * from about where sn=1 ";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $data[0];
	}
}