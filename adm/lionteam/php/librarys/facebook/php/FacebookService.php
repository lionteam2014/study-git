<?
/**
 * 獨立出處理 facebook 登入機制
 * 正在整理中未調整完畢
 * */
class FacebookService {
	/** 主要負責的頁面導向 javascript ，處理 json 物件 */
	public static $jsonHandler = "js_facebook_handler.php";
	
	/** 應用程式 ID / API 鑰匙 */
	private static $app_id = "134951176683872";
	 
	/** 應用程式密鑰 */
	private static $app_secret = "8ec5a9f44e224b4e152e92c84e39355c";
	
	/**
	 * 可以要求使用者未公開資訊，會讓使用者自行確認同意後才可取得。  </br>
	 * 例如: scope=user_birthday,read_stream   </br>
	 * 官方說明頁:https://developers.facebook.com/docs/concepts/login/permissions-login-dialog/
	 * 20140311改版
	 * https://developers.facebook.com/docs/facebook-login/permissions
	 */
	const scope = "email"; // 要取得使用者email
	
	private static $json;
	
	public static function init(array $options){
		FacebookService::$app_id = $options["app_id"];
		FacebookService::$app_secret = $options["app_secret"];

		if( array_key_exists("jsonHandler", $options) )
			FacebookService::$jsonHandler = $options["jsonHandler"];
		
	}

	/**
	 * facebook 登入的URL
	 * @param unknown $code
	 * @return string
	 */
	public static function getFacebookLoginRedectorURL($code){
		if(null == $code) {
// 			$lastndexOf = strrpos($_SERVER['PHP_SELF'],"/",0);
// 			$dir = substr($_SERVER['PHP_SELF'],0,$lastndexOf);
			
			// facebook 登入後要轉導頁面
			$redirect= "http://".$_SERVER['HTTP_HOST']."/".FacebookService::$jsonHandler;
	
			// Redirect to Login Dialog
			$redector = "https://www.facebook.com/dialog/oauth?client_id=".FacebookService::$app_id."&redirect_uri=$redirect&state=".$_SERVER['PHP_SELF']."&scope=".FacebookService::scope;
		}
		return $redector;
	}
	
	/**
	 *  取得 登入後使用者 json 字串
	 * @param unknown $code Facebook 登入成功後轉回頁面會帶的 $_GET['code'] 參數
	 * @return string Facebook response json
	 * 
	 *  p.s. 需要打開 curl 跟  openssl
	 */
	public static function getFacebookUserJson($code){
		if(!extension_loaded('openssl')){
			throw new Exception('This app needs the Open SSL PHP extension.');
		}
		if(null != $code)
		{
			$lastndexOf = strrpos($_SERVER['PHP_SELF'],"/",0);
			$dir = substr($_SERVER['PHP_SELF'],0,$lastndexOf);
			$parentURL= "http://".$_SERVER['HTTP_HOST'].$dir; // 路徑紀錄
			$redirect= $parentURL."/".FacebookService::$jsonHandler;
			// 取得fb access_token
			$token_url="https://graph.facebook.com/oauth/access_token?client_id=".FacebookService::$app_id."&redirect_uri=".$redirect."&client_secret=".FacebookService::$app_secret."&code=".$code;

// 			$response = file_get_contents($token_url);
			
// 			$response = self::https_request($token_url);
			ob_start();
			$response = ob_get_contents($token_url);
			ob_end_clean();
			

			$params = NULL;
			parse_str($response, $params);
	
			$_SESSION['access_token'] = $params['access_token'];
	
			$graph_url = "https://graph.facebook.com/me?access_token=".$params['access_token'];
			// 取得使用者資訊
// 			FacebookService::$json = file_get_contents($graph_url);
			FacebookService::$json = self::https_request($graph_url);
			
			ob_start();
			FacebookService::$json = ob_get_contents($graph_url);
			ob_end_clean();

			return FacebookService::$json;
		}
	}
	
	public static function https_request($url){
        if (!function_exists('curl_init')) {
            throw new Exception('server not install curl');
        }
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    curl_setopt($ch, CURLOPT_HEADER, false);
// 	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_REFERER, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $result = self::curl_exec_follow($ch);
	    curl_close($ch);
	    return $result;
	}
	
	private static function curl_exec_follow($ch, &$maxredirect = null) {
  
	  // we emulate a browser here since some websites detect
	  // us as a bot and don't let us do our job
	  $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
	                " Gecko/20041107 Firefox/1.0";
	  curl_setopt($ch, CURLOPT_USERAGENT, $user_agent );
	
	  $mr = $maxredirect === null ? 5 : intval($maxredirect);
	
	  if (ini_get('open_basedir') == '' && ini_get('safe_mode') == 'Off') {
	
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
	    curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	  } else {
	    
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	
	    if ($mr > 0)
	    {
	      $original_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
	      $newurl = $original_url;
	      
	      $rch = curl_copy_handle($ch);
	      
	      curl_setopt($rch, CURLOPT_HEADER, true);
	      curl_setopt($rch, CURLOPT_NOBODY, true);
	      curl_setopt($rch, CURLOPT_FORBID_REUSE, false);
	      do
	      {
	        curl_setopt($rch, CURLOPT_URL, $newurl);
	        $header = curl_exec($rch);
	        if (curl_errno($rch)) {
	          $code = 0;
	        } else {
	          $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);
	          if ($code == 301 || $code == 302) {
	            preg_match('/Location:(.*?)\n/', $header, $matches);
	            $newurl = trim(array_pop($matches));
	            
	            // if no scheme is present then the new url is a
	            // relative path and thus needs some extra care
	            if(!preg_match("/^https?:/i", $newurl)){
	              $newurl = $original_url . $newurl;
	            }   
	          } else {
	            $code = 0;
	          }
	        }
	      } while ($code && --$mr);
	      
	      curl_close($rch);
	      
	      if (!$mr)
	      {
	        if ($maxredirect === null)
	        trigger_error('Too many redirects.', E_USER_WARNING);
	        else
	        $maxredirect = 0;
	        
	        return false;
	      }
	      curl_setopt($ch, CURLOPT_URL, $newurl);
	    }
	  }
	  return curl_exec($ch);
	}
	
	/**
	 * 可以針對 json 做處裡，寫入到資料庫後轉導頁面等等的，自行擴充內容
	 */
	 public static function facebookJsonHandler(){
	 	$obj = json_decode(FacebookService::$json);
	 	/** 可以在此寫入資料庫等等
	 	 * ....
	 	 * ....
	 	 */
	 	return $obj;
	 }	 
}