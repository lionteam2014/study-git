<?php
/**
 * 實用工具包(把好用的 function static 收在此處隨時可以用 )
 * @author raybird
 *
 */
class Toolkit {
	
	/**
	 * 處理中英文數字混雜字串切割處裡
	 * 
	 * @param unknown $string
	 *        	字串
	 * @param unknown $start
	 *        	開始位置
	 * @param unknown $length
	 *        	結束位置
	 * @return string
	 */
	public static function substrUtf8($string, $start, $length) { // by aiou
		$chars = $string;
		// echo $string[0].$string[1].$string[2];
		$i = 0;
		do {
			if (preg_match ( "/[0-9a-zA-Z]/", $chars [$i] )) { // 純英文
				$m ++;
			} else {
				$n ++;
			} // 非英文字節,
			$k = $n / 3 + $m / 2;
			$l = $n / 3 + $m; // 最終截取長度；$l = $n/3+$m*2？
			$i ++;
		} while ( $k < $length );
		$str1 = mb_substr ( $string, $start, $l, 'utf-8' ); // 保證不會出現亂碼
		return $str1;
	}
	
	/**
	 * 外部縮圖機制，需要 depend on timthump.php.
	 * <br>
	 * 官方網址 http://www.binarymoon.co.uk/projects/timthumb/
	 * 
	 * @param unknown $width        	
	 * @param unknown $height        	
	 * @param unknown $img        	
	 * @param unknown $zc        	
	 * @return string
	 */
	public static function getResizeImg($width, $height, $img, $zc = 1) {
		// return "timthumb.php?src=".SERVER_URL."$img&h=$height&w=$width&zc=1";
		return "timthumb.php?src=$img&h=$height&w=$width&zc=$zc";
	}
	
	/**
	 * 取的圖檔指定寬高下等比例縮圖寬高。 <br>
	 * 
	 * @param unknown $css_width
	 *        	頁面顯示最大寬度
	 * @param unknown $css_height
	 *        	頁面顯示最大高度
	 * @param unknown $img
	 *        	檔案路徑
	 * @return multitype:unknown Ambigous <unknown, multitype:>
	 */
	public static function getProportionalImgInfo($css_width, $css_height, $img) {
		/**
		 * 使用方式 **\
		 * $info = getProportionalImgInfo("244","198",$img);
		 * <img src="<?=$img>" width="<?=$info['width']?>" height="<?=$info['height']?>">
		 * \** 使用方式 *
		 */
		$info = array ();
		// 真實尺寸
		list ( $img_width, $img_height ) = getimagesize ( $img );
		if ($css_width > $img_width) {
			$width = $img_width;
		} else {
			$width = $css_width;
		}
		
		if ($css_height > $img_height) {
			$height = $img_height;
		} else {
			$height = $css_height;
		}
		
		$info ['width'] = $width;
		$info ['height'] = $height;
		return $info;
	}
	
	/**
	 * 跳脫字元整理
	 * @param unknown $obj
	 * @return multitype:number string |string|unknown
	 */
	public static function handlerSlashes($obj){
		if(!get_magic_quotes_gpc()){
		  	//手動跳脫
			if(is_array($obj)){
				$_obj = array();
				foreach ($obj as $key => $value) {
					if(is_array($value)){
						$_obj[$key] = Toolkit::handlerSlashes($value);
					}else{
						$_obj[$key] = addslashes($value);
					}
				}
				return $_obj;
			}else{
				return addslashes($obj);
			}
		}
		return $obj;
	}
	
	/**
	 * send email
	 * @param unknown $Mail_From 發信者
	 * @param unknown $Mail_To 發信給
	 * @param unknown $subject 主題
	 * @param unknown $content 內容
	 * @return boolean
	 */
	public static function mail($Mail_From, $Mail_To, $subject, $content){
		$subject = "=?UTF-8?B?" . base64_encode($subject) . " ?=";
		$header_str = "Content-Type:text/html;charset=UTF-8\r\nFrom:$Mail_From\r\nreply-to:$Mail_From\r\n";
		
		return $return_arg = mail($Mail_To, $subject, $content, $header_str);
	}
	
	/**
	 * 取 php 載入後的結果
	 * 範例:
	 $options = array(
	 "php" => "goods_notice.php", // 要跑的 php 頁面
	 "DB_class" => $DB_class,
	 "order_main" =>$m_row,
	 "customer" => $c_row,
	 "order_list_rs" => $listrs);
	 $content = execPHP($options);
	
	 此時 goods_notice.php 可以直接使用 $options 取值，
	 或者直接用 key 取值。
	 例如:
	 $DB_class, $order_main ...
	 *
	 * @param unknown $options 可以給 php 頁面必須的資料
	 * @return string
	 */
	public static function execPHP($options){
	
		if(!isset($options["php"])){
			throw new Exception("請設定 php 頁面");
		}
	
		if(!file_exists($options["php"])){
			throw new Exception("設定 php 頁面，不存在或路徑錯誤:".$options["php"]);
		}
	
	
	
		foreach ($options as $key => $value) {
			$$key = $value;
		}
	
		ob_start();
		include $php;
		$buffer = ob_get_clean();
		return $buffer;
	}

	public static function maingun(){
// 		require 'mailgun-php-1.7/vendor/autoload.php';
// 		use Mailgun\Mailgun;
		
// 		# Instantiate the client.
// 		$mgClient = new Mailgun('key-04w11yyksbrp1czmdx1ktiglhg-39va3');
// 		$domain = "sandbox74547.mailgun.org";
		
// 		# Make the call to the client.
// 		$result = $mgClient->sendMessage("$domain",
// 		array('from'    => 'test <service@test.org>',
// 		'to'      => 'Kevin <a691228@gmail.com>',
// 		'subject' => '中文-許功蓋',
// 		'text'    => 'Testing some Mailgun awesomness!',
// 		'html'    => '<html>中文許工蓋</html>'));
	}
	
}