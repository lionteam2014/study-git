<?php

/**
 * @author raybird
 *
 */
class System extends \Controller {
	
	/*
	 * (non-PHPdoc) @see Controller::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	/*
	 * (non-PHPdoc) @see Controller::after()
	 */
	public function after() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::destory()
	 */
	public function destory() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::before()
	 */
	public function before() {
		// TODO Auto-generated method stub
	}
	
	
	public function index(){
		Loader::load("lionteam/php/system/SystemModel.php");
		$model = new SystemModel();

		$data = $model->getSystem();
		
		Loader::load("lionteam/php/system/SystemView.php");
		$view = new SystemView();
		
		$view->render("lionteam/php/system/views/index.php",$data);
		
	}
	
	
	public function getSystem(){
		Loader::load("lionteam/php/system/SystemModel.php");
		$model = new SystemModel();
		
		$data = $model->getSystem();
	}
	
	
	public function updateDefault(){
		Loader::load("system/SystemModel.php");
		$model = new SystemModel();
		
		$result = $model->updateDefault();
		
		$this->responseSimpleJson($result);
		
	}
	
	public function updateEC(){
		Loader::load("system/SystemModel.php");
		$model = new SystemModel();
		
		$result = $model->updateEC();
		
		$this->responseSimpleJson($result);
	}
}