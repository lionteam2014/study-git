<?php

/**
 * @author raybird
 *
 */
class News extends \Controller {
	
	/*
	 * (non-PHPdoc) @see Controller::__construct()
	 */
	public function __construct() {
		// TODO Auto-generated method stub
		parent::__construct ();
	}
	
	/*
	 * (non-PHPdoc) @see Controller::after()
	 */
	public function after() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::destory()
	 */
	public function destory() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Controller::before()
	 */
	public function before() {
		// TODO Auto-generated method stub
	}
	
	public function index(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$data = $model->getAllNews();
		
		Loader::load("lionteam/php/news/NewsView.php");
		$view = new NewsView();
		$view->render("lionteam/php/news/views/list.php", $data);
	}
	
	public function add(){
		Loader::load("lionteam/php/news/NewsView.php");
		$view = new NewsView();
		$view->render("lionteam/php/news/views/add.php");
	}
	
	public function add_action(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$result = $model->saveNews();
		
		$this->responseSimpleJson($result);
	
	}
	
	public function show(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$data = $model->getNews();
		
		Loader::load("lionteam/php/news/NewsView.php");
		$view = new NewsView();
		$view->render("lionteam/php/news/views/show.php", $data);
		
	}
	
	
	public function selected_action(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$result = $model->selected_action();
		
		$this->responseSimpleJson($result);
	}
	
	
	public function modify(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$data = $model->getNews();
		
		Loader::load("lionteam/php/news/NewsView.php");
		$view = new NewsView();
		$view->render("lionteam/php/news/views/modify.php", $data);
	}
	
	public function modify_action(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$result = $model->saveNews();
		
		$this->responseSimpleJson($result);
	}
	
	public function sort_action(){
		Loader::load("lionteam/php/news/NewsModel.php");
		$model = new NewsModel();
		
		$result = $model->sort();
		
		$this->responseSimpleJson($result);
	}
}