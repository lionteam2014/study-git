<?php
/**
 * html 空白字
 * @param int $time
 */
function html_space($time){
	
	if(is_int($time)){
		$string = "";
		for ($i = 0; $i < $time; $i++) {
			$string .= "&nbsp;";
		}	
		return $string;
	}
	return "";
}

/**
 * 檔案上傳器
 * http://www.w3schools.com/php/php_file_upload.asp
 */
function uploadHandler($name, $storeDir){
	
	$FILES = $_FILES[$name];
	
	$allowedExts = array("gif", "jpeg", "jpg", "png");
	$temp = explode(".", $FILES["name"]);
	$extension = end($temp);
	$type = $FILES["type"];
	if ((($type == "image/gif")
			|| ($type == "image/jpeg")
			|| ($type == "image/jpg")
			|| ($type == "image/pjpeg")
			|| ($type == "image/x-png")
			|| ($type == "image/png"))
			&& ($FILES["size"] < 2*1024*1024)
			&& in_array($extension, $allowedExts))
	{
		if ($FILES["error"] > 0)
		{
			return false;
		}
		else
		{

			$save_name = time();
			$save_name .= ".$extension";
			if (file_exists("$storeDir/" . $save_name))
			{
				return false;
			}
			else
			{
				move_uploaded_file($FILES["tmp_name"], "$storeDir/" . $save_name);
				return $save_name;
			}
		}
	}
	else
	{
		return false;
	}
}

/**
 * 寄送 html 內容。若是遇到 outlook 收件者則 header 的換行 \r\n 要換成 \n
 * @param unknown $from 發信 email
 * @param unknown $to 收信 email
 * @param unknown $subject 主旨
 * @param unknown $content html 內容
 * @param string $cc 附件 email
 * @return boolean 寄件結果
 */
function mailHtml($from, $to, $subject, $content, $cc=null){
	$content=stripslashes($content);//信件內容
	
	$subject = "=?UTF-8?B?" . base64_encode($subject) . " ?=";

	$body = '<html><body>';
	$body .= $content;
	$body .= '</body></html>';
	
	$headers = "From: " . strip_tags($from) . "\r\n";
	$headers .= "Reply-To: ". strip_tags($from) . "\r\n";
	if(isset($cc)){
		$headers .= "CC: $cc\r\n";
	}
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	
	
	return mail($to, $subject, $body, $headers);
}

/**
 * 日期格式到毫秒
 * datetime2millisecond('Y-m-d H:i:s:u'); // 2010-11-15 21:21:00:987
 * @param unknown $format
 * @param string $utimestamp
 * @return string
 * 
 * http://stackoverflow.com/questions/4184769/how-to-get-current-time-in-ms-in-php
 */
function datetime2millisecond($format, $utimestamp = null) {
	if (is_null($utimestamp))
		$utimestamp = microtime(true);

	$timestamp = floor($utimestamp);
	$milliseconds = round(($utimestamp - $timestamp) * 1000000);

	return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
}

/**
 * 利用 timthumb.php 進行前台頁面縮圖
 * http://www.binarymoon.co.uk/projects/timthumb/
 * @param array $options
 */
function pageImage(array $options){
	$width = $options["width"];
	$height = $options["height"];
	$path = $options["path"];
	
	return "timthumb.php?src=$path&h=$height&w=$width&zc=1";
}